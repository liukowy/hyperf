<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\DbConnection\Model\Model;
use Hyperf\ModelCache\Cacheable;//缓存;
use Hyperf\ModelCache\CacheableInterface; //缓存接口

/**
 */
class TabelName extends Model
{
    use Cacheable;
    //查询单个缓存

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'tabel_name';
    public $timestamps = false;//不设置维护时间;
    protected $dateFormat = 'U';//时间戳的格式;
    //如果您需要不希望保持 datetime 格式的储存，或者希望对时间做进一步的处理，您可以通过在模型内重写fromDateTime($value) 方法实现。
    public function fromDateTime($value)
    {
        //在这里重新;
    }
    //如果你需要自定义存储时间戳的字段名，可以在模型中设置 CREATED_AT 和 UPDATED_AT 常量的值来实现，其中一个为 null，则表明不希望 ORM 处理该字段：
    const CREATED_AT = 'create_date';

    // 重新加载模型
//你可以使用 fresh 和 refresh 方法重新加载模型。 fresh 方法会重新从数据库中检索模型。现有的模型实例不受影响：  模型都是单例模式  refresh 方法使用数据库中的新数据重新赋值现有模型。此外，已经加载的关系会被重新加载：
//$user = User::query()->where('name','Hyperf')->first();
//
//$user->name = 'Hyperf2';
//
//$user->refresh();

//echo $user->name; // Hyperf
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name','gender','created_at','updated_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id'=>'integer','gender'=>'integer'];
}