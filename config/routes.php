<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

use Hyperf\HttpServer\Router\Router;
use App\Middleware\Auth\FooMiddleware;

//这个路由前面必须要加斜杠 /
//Router::get('/nihao',function(){
//    return '你好吗';
//});

//路由群组
//Router::addGroup('/user/',function (){
//    Router::get('index','App\Controller\UserController@index');
//});

//路由参数 本框架定义的路由参数必须和控制器参数键名、类型保持一致，否则控制器无法接受到相关参数
//这三种方法的路由都可以
//Router::get('/{id}', 'App\Controller\IndexController@index');
//Router::addRoute(['GET', 'POST', 'HEAD'], '/', 'App\Controller\IndexController@index');
//关于中间件----非全局的中间件；全局的在配置文件中配置--；
//Router::get('/', [\App\Controller\IndexController::class, 'index'], ['middleware' => [FooMiddleware::class]]);
// 该 Group 下的所有路由都将应用配置的中间件
//Router::addGroup('/',function () {
//    Router::get('index', [\App\Controller\IndexController::class, 'index']);
//},
//    ['middleware' => [FooMiddleware::class]]
//);
//
//Router::get('/two','App\Controller\TwoController@index');//第二个测试的;

Router::addServer('ws',function(){
    Router::get('/swoole','App\Controller\WebSocketController');
});

Router::get('/clienttest','App\Controller\TwoController@clienttest');//
Router::get('/tasktest','App\Controller\TwoController@tasktest');//第二个测试的;