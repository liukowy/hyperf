<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

return [
    Hyperf\AsyncQueue\Process\ConsumerProcess::class,//组件已经提供了默认子进程，只需要将它配置到 config/autoload/processes.php 中即可。---已经配置；---异步队列--
    Hyperf\Crontab\Process\CrontabDispatcherProcess::class,//任务调度；
];
