<?php

declare(strict_types=1);

use Hyperf\JsonRpc\JsonRpcPoolTransporter; //连接池--jsonrpc使用
use Hyperf\JsonRpc\JsonRpcTransporter;//连接池--jsonrpc使用
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

return [
    'InnerHttp'=>Hyperf\HttpServer\Server::class,
//    JsonRpcTransporter::class => JsonRpcPoolTransporter::class,//连接池--jsonrpc使用

];

