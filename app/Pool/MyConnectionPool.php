<?php

use Hyperf\Contract\ConnectionInterface;
use Hyperf\Pool\Pool;
use CoTest;

class MyConnectionPool extends Pool
{
    public function createConnection(): ConnectionInterface
    {
        return new MyConnection();
    }
}