<?php
// header('Content-type: application/json');
error_reporting(E_ALL^E_NOTICE^E_WARNING);
date_default_timezone_set('PRC');

function warrior_menu(){
    $items = array();
    //获取openid,sessionkey
    $items['getopenid'] = array(
        'page callback' => 'warrior_getopenid',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取用户微信信息
    $items['getwxinfo'] = array(
        'page callback' => 'warrior_getwxinfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //登陆
    $items['login'] = array(
        'page callback' => 'warrior_login',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取首页KV
    $items['getkvlist'] = array(
        'page callback' => 'warrior_getkvlist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取首页最新上架
    $items['getindexintegralgoodslist'] = array(
        'page callback' => 'warrior_getindexintegralgoodslist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取首页限时抢购商品
    $items['getindexpromptgoodslist'] = array(
        'page callback' => 'warrior_getindexpromptgoodslist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取积分商品列表
    $items['getintegralgoodslist'] = array(
        'page callback' => 'warrior_getintegralgoodslist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取积分商品详细
    $items['getintegralgoodsinfo'] = array(
        'page callback' => 'warrior_getintegralgoodsinfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取限时抢购商品
    $items['getpromptgoodslist'] = array(
        'page callback' => 'warrior_getpromptgoodslist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取限时抢购商品详细
    $items['getpromptgoodsinfo'] = array(
        'page callback' => 'warrior_getpromptgoodsinfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取秒杀商品列表
    $items['getkillgoodslist'] = array(
        'page callback' => 'warrior_getkillgoodslist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取秒杀商品详细
    $items['getkillgoodsinfo'] = array(
        'page callback' => 'warrior_getkillgoodsinfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取用户信息
    $items['getuserinfo'] = array(
        'page callback' => 'warrior_getuserinfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取用户服务申请状态
    $items['getapplicationstatus'] = array(
        'page callback' => 'warrior_getapplicationstatus',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //提交服务申请
    $items['submitapplication'] = array(
        'page callback' => 'warrior_submitapplication',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //兑换积分商品
    $items['submitintegralgoodsorder'] = array(
        'page callback' => 'warrior_submitintegralgoodsorder',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //购买限时抢购商品
    $items['submitpromptgoodsorder'] = array(
        'page callback' => 'warrior_submitpromptgoodsorder',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取积分商品订单列表
    $items['getintegralgoodsorderlist'] = array(
        'page callback' => 'warrior_getintegralgoodsorderlist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //取消积分订单
    $items['cancelintegralgoodsorder'] = array(
        'page callback' => 'warrior_cancelintegralgoodsorder',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取限时抢购商品订单列表
    $items['getpromptgoodsorderlist'] = array(
        'page callback' => 'warrior_getpromptgoodsorderlist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取积分商品订单详情
    $items['getintegralgoodsorderinfo'] = array(
        'page callback' => 'warrior_getintegralgoodsorderinfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取限时抢购商品订单详情
    $items['getpromptgoodsorderinfo'] = array(
        'page callback' => 'warrior_getpromptgoodsorderinfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //上传批发商订单excel页面
    $items['admin/importorderlist'] = array(
        'page callback' => 'warrior_importorderlist',
        'access callback' => 'user_access',
        'access arguments' => array('administer users'),
        'type' => MENU_CALLBACK,
    );
    //导入批发商订单接口
    $items['importorderexcel'] = array(
        'page callback' => 'warrior_importorderexcel',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //上传零售商/批发商页面
    $items['admin/importuser'] = array(
        'page callback' => 'warrior_importuser',
        'access callback' => 'user_access',
        'access arguments' => array('administer users'),
        'type' => MENU_CALLBACK,
    );
    //导入零售商/批发商接口
    $items['importuserexcel'] = array(
        'page callback' => 'warrior_importuserexcel',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //上传零售商/批发商积分页面
    $items['admin/importuserintegral'] = array(
        'page callback' => 'warrior_importuserintegral',
        'access callback' => 'user_access',
        'access arguments' => array('administer users'),
        'type' => MENU_CALLBACK,
    );
    //导入零售商/批发商积分接口
    $items['importuserintegralexcel'] = array(
        'page callback' => 'warrior_importuserintegralexcel',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //上传零售商/批发商返点页面
    $items['admin/importuserprice'] = array(
        'page callback' => 'warrior_importuserprice',
        'access callback' => 'user_access',
        'access arguments' => array('administer users'),
        'type' => MENU_CALLBACK,
    );
    //导入零售商/批发商返点接口
    $items['importuserpriceexcel'] = array(
        'page callback' => 'warrior_importuserpriceexcel',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取用户产品进货订购详情
    $items['getproductorderlist'] = array(
        'page callback' => 'warrior_getproductorderlist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取订购条件
    $items['getproductcondition'] = array(
        'page callback' => 'warrior_getproductcondition',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取用户收获信息列表
    $items['getusergaininfo'] = array(
        'page callback' => 'warrior_getusergaininfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //新增用户收获信息
    $items['addusergaininfo'] = array(
        'page callback' => 'warrior_addusergaininfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //修改用户收获信息
    $items['editusergaininfo'] = array(
        'page callback' => 'warrior_editusergaininfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //设置用户默认地址
    $items['setdefaultaddress'] = array(
        'page callback' => 'warrior_setdefaultaddress',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //删除用户收获信息
    $items['deleteusergaininfo'] = array(
        'page callback' => 'warrior_deleteusergaininfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //个人积分明细查询
    $items['getuserintegrallist'] = array(
        'page callback' => 'warrior_getuserintegrallist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //上传订单状态
    $items['admin/importorderstatuslist'] = array(
        'page callback' => 'warrior_importorderstatuslist',
        'access callback' => 'user_access',
        'access arguments' => array('administer users'),
        'type' => MENU_CALLBACK,
    );
    //导入订单状态接口
    $items['importorderstatusexcel'] = array(
        'page callback' => 'warrior_importorderstatusexcel',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //删除数据
    $items['deletedata'] = array(
        'page callback' => 'warrior_deletedata',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取供应商/零售商id
    $items['getuserid'] = array(
        'page callback' => 'warrior_getuserid',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //获取用户信息
    $items['getusername'] = array(
        'page callback' => 'warrior_getusername',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //更新零售商/批发商总积分页面
    $items['admin/importusertotalintegral'] = array(
        'page callback' => 'warrior_importusertotalintegral',
        'access callback' => 'user_access',
        'access arguments' => array('administer users'),
        'type' => MENU_CALLBACK,
    );
    //更新零售商/批发商总积分接口
    $items['importusertotalintegralexcel'] = array(
        'page callback' => 'warrior_importusertotalintegralexcel',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //下载零售商积分商城报表页面
    $items['downloadllsjfscbbpage'] = array(
        'page callback' => 'warrior_downloadllsjfscbbpage',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //下载零售商积分商城报表
    $items['downloadllsjfscbb'] = array(
        'page callback' => 'warrior_downloadllsjfscbb',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //下载批发商积分商城报表页面
    $items['downloadpffjfscbbpage'] = array(
        'page callback' => 'warrior_downloadpffjfscbbpage',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    //下载批发商积分商城报表
    $items['downloadpffjfscbb'] = array(
        'page callback' => 'warrior_downloadpffjfscbb',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );

    //提交秒杀订单
    $items['submitkillgoodsorder'] = array(
        'page callback' => 'warrior_submitkillgoodsorder',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );

    //获取秒杀订单列表
    $items['getkillgoodsorderlist'] = array(
        'page callback' => 'warrior_getkillgoodsorderlist',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );

    //获取秒杀订单详情
    $items['getkillgoodsorderinfo'] = array(
        'page callback' => 'warrior_getkillgoodsorderinfo',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );

    //用户积分订单导入页面
    $items['admin/importintegralorderpage'] = array(
        'page callback' => 'warrior_importintegralorderpage',
        'access callback' => 'user_access',
        'access arguments' => array('administer users'),
        'type' => MENU_CALLBACK,
    );
    //用户积分订单导入页面
    $items['importintegralorder'] = array(
        'page callback' => 'warrior_importintegralorder',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );

    //
    $items['warriortest'] = array(
        'page callback' => 'warrior_test',
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
    );
    return $items;
}

function warrior_importintegralorderpage(){
    return theme('warrior_import_integralorderpage');
}

function warrior_importintegralorder(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        die();
    } else {
        // if (file_exists("sites/all/modules/warrior/userexcel/" . $_FILES["file"]["name"])) {
        //     echo '<script>alert("上传文件已存在！");location.href="/admin/importuser";</script>';
        //     die();
        // } else {
        move_uploaded_file($_FILES["file"]["tmp_name"], "sites/all/modules/warrior/integralorderexcel/" . $_FILES["file"]["name"]);
        // }
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel5.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel2007.php');
    // $data = new Spreadsheet_Excel_Reader();
    // $data->read("excel/" . $_FILES["file"]["name"]);
    $reader = PHPExcel_IOFactory::createReader('Excel2007'); //设置以Excel5格式(Excel97-2003工作簿)
    $PHPExcel = $reader->load("sites/all/modules/warrior/integralorderexcel/" . $_FILES["file"]["name"]); // 载入excel文件
    $sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
    $highestRow = $sheet->getHighestRow(); // 取得总行数
    $highestColumm = $sheet->getHighestColumn(); // 取得总列数
    /** 循环读取每个单元格的数据 */
    $num=0;
    $wrongnum=0;
    for ($row = 2; $row <= $highestRow; $row++){//行数是以第1行开始
        $orderid=$sheet->getCell('A'.$row)->getValue();//订单号
        $uid=db_query("SELECT entity_id FROM field_data_field_employeesinfo_id WHERE field_employeesinfo_id_value='".$sheet->getCell('H'.$row)->getValue()."'")->fetchColumn();
        $nid=db_query("SELECT entity_id FROM field_data_field_integralgoods_id WHERE field_integralgoods_id_value='".$sheet->getCell('C'.$row)->getValue()."'")->fetchColumn();
        $num=$sheet->getCell('E'.$row)->getValue();
        $goodstotalintegral=$sheet->getCell('F'.$row)->getValue();
        $integralgoodsprice=db_query("SELECT field_integralgoods_price_value FROM field_data_field_integralgoods_price WHERE entity_id='".$nid."'")->fetchColumn();
        $totalintegralgoodsprice=$num*$integralgoodsprice;
        $name=$sheet->getCell('I'.$row)->getValue();
        $mobile=$sheet->getCell('J'.$row)->getValue();
        $province=$sheet->getCell('K'.$row)->getValue();
        $city=$sheet->getCell('L'.$row)->getValue();
        $area=$sheet->getCell('M'.$row)->getValue();
        $address=$sheet->getCell('N'.$row)->getValue();
        $serialnum=$sheet->getCell('A'.$row)->getValue();
        $status=$sheet->getCell('P'.$row)->getValue();
        $remark='';

        $n = intval(($sheet->getCell('O'.$row)->getValue() - 25569) * 3600 * 24);     //转换成1970年以来的秒数
        // echo gmdate('Y-m-d H:i:s',$n);              //格式化时间,不是用date哦, 时区相差8小时的
        $created=strtotime(gmdate('Y-m-d H:i:s',$n));

        $node = new stdClass;
        $node->type = "exchangeorder";
        $node->language = "und";
        $node->title = $orderid;
        $node->uid = $uid;
        $node->field_exchangeorder_user['und'][0]['nid'] = $uid;
        $node->field_exchangeorder_goods['und'][0]['nid'] = $nid;
        $node->field_exchangeorder_num['und'][0]['value'] = $num;
        $node->field_exchangeorder_integral['und'][0]['value'] = $goodstotalintegral;
        $node->field_exchangeorder_totalprice['und'][0]['value'] = $totalintegralgoodsprice;
        $node->field_exchangeorder_name['und'][0]['value'] = $name;
        $node->field_exchangeorder_mobile['und'][0]['value'] = $mobile;
        $node->field_exchangeorder_province['und'][0]['value'] = $province;
        $node->field_exchangeorder_city['und'][0]['value'] = $city;
        $node->field_exchangeorder_area['und'][0]['value'] = $area;
        $node->field_exchangeorder_address['und'][0]['value'] = $address;
        $node->field_exchangeorder_serialnum['und'][0]['value'] = $serialnum;
        $node->field_exchangeorder_status['und'][0]['value'] = $status;
        $node->field_exchangeorder_remark['und'][0]['value'] = $remark;
        $node->created=$created;
        node_save($node);

        $userinfo=node_load($uid);
        $userinfo->field_employeesinfo_integral['und'][0]['value']=$userinfo->field_employeesinfo_integral['und'][0]['value']-$goodstotalintegral;
        node_save($userinfo);

        $goods=node_load($nid);
        $goods->field_integralgoods_exchangenum['und'][0]['value']=$goods->field_integralgoods_exchangenum['und'][0]['value']+$num;
        node_save($goods);

        db_query("INSERT INTO warrior_user_integral_list VALUES (NULL,'礼品兑换','".$userinfo->field_employeesinfo_id['und'][0]['value']."','".$userinfo->title."','".(-$goodstotalintegral)."','".$created."','".$userinfo->field_employeesinfo_integral['und'][0]['value']."')");
    }
    echo '<script>alert("导入成功");location.href="?q=admin/importintegralorderpage"</script>';
    exit();
}

function warrior_test(){
    $arrNid=db_query("select a.nid from node a
inner join field_data_field_snapuporder_activitynid b on a.nid=b.entity_id
where a.type='snapuporder' and (b.field_snapuporder_activitynid_nid='44750' or b.field_snapuporder_activitynid_nid='44751') and a.created>1564970400")->fetchAll();
    var_dump($arrNid);die();
    foreach ($arrNid as $key => $value) {
        // $node=node_load($value->nid);
        // $promptgoodsinfo=node_load($node->field_snapuporder_goods['und'][0]['nid']);
        // $promptgoodsinfo->field_promptgoods_buynum['und'][0]['value']=$promptgoodsinfo->field_promptgoods_buynum['und'][0]['value']-$node->field_snapuporder_num['und'][0]['value'];
        // node_save($promptgoodsinfo);
        // node_delete($value->nid);
    }
    die('done');
    // $arrnid=db_query("SELECT nid FROM node WHERE type='integralgoods'")->fetchAll();
    // foreach ($arrnid as $key => $value) {
    //     $node=node_load($value->nid);
    //     $node->field_field_integralgoods_asc['und'][0]['value']=100;
    //     node_save($node);
    // }
    // die('done');
    // $userid=$_REQUEST['userid'];
    // $mobile=$_REQUEST['mobile'];
    // $openid=$_REQUEST['openid'];
    // $nickname=base64_encode($_REQUEST['nickname']);
    // $headurl=$_REQUEST['headurl'];
    // $gender=$_REQUEST['gender'];
    // if($userid && $mobile && $openid){
    //     $id = db_query("SELECT a.nid FROM node a
    //                     INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
    //                     INNER JOIN field_data_field_employeesinfo_mobile c ON c.entity_id=a.nid
    //                     WHERE b.field_employeesinfo_id_value='".$userid."' AND c.field_employeesinfo_mobile_value='".$mobile."'")->fetchColumn();
    //     echo $id;die();
    //     if($id){
    //         // $bindid = db_query("SELECT a.nid FROM node a
    //         //             INNER JOIN field_data_field_employeesinfo_openid b ON b.entity_id=a.nid
    //         //             WHERE b.field_employeesinfo_openid_value='".$openid."'")->fetchColumn();
    //         // if($id!=$bindid){
    //         //     result(3,'账号与绑定微信号不符');
    //         // }
    //         $node=node_load($id);
    //         if(!$node->field_employeesinfo_openid['und'][0]['value'] || $openid==$node->field_employeesinfo_openid['und'][0]['value']){
    //             $node->field_employeesinfo_sex['und'][0]['value']=$gender;
    //             $node->field_employeesinfo_openid['und'][0]['value']=$openid;
    //             $node->field_employeesinfo_nickname['und'][0]['value']=$nickname;
    //             $node->field_employeesinfo_headurl['und'][0]['value']=$headurl;
    //             $node->field_employeesinfo_dotime['und'][0]['value']=date('Y-m-d H:i:s',time());
    //             node_save($node);
    //             result(1,'登陆成功');
    //         }else{
    //             result(3,'账号与绑定微信号不符');
    //         }
    //     }else{
    //         result(2,'用户不存在');
    //     }
    // }else{
    //     result(40001,'缺少参数');
    // }
    // $arrnid=db_query("SELECT nid FROM node WHERE type='integralgoods'")->fetchAll();
    // foreach ($arrnid as $key => $value) {
    //     $node=node_load($value->nid);
    //     $node->status=1;
    //     node_save($node);
    // }
    // die('done');
}

function warrior_submitkillgoodsorder(){
    $openid=$_REQUEST['openid'];
    $activitynid=$_REQUEST['activitynid'];
    $killgoodsid=$_REQUEST['killgoodsid'];
    $num=$_REQUEST['num'];
    $userid=$_REQUEST['userid'];//新增

    if($openid && $activitynid && $killgoodsid && $num && $userid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $userinfo=node_load($uid);
        if($userinfo->field_employeesinfo_type['und'][0]['value']=='批发商'){
            result(5,'批发商不能参与抢购');
        }

        $killactivityinfo=node_load($activitynid);
        $killgoodsinfo=node_load($killgoodsid);

        $fp = fopen("lockceshi.txt", "w");

        if(flock($fp,LOCK_EX))
        {

            if($killgoodsinfo->field_killgoods_inventory['und'][0]['value']<$num)
            {
                fclose($fp);
                result(3,'库存不足');
            }

            // $arrNum=db_query("SELECT c.field_killorder_num_value FROM node a
            //                  INNER JOIN field_data_field_killorder_killactivity b ON b.entity_id=a.nid
            //                  INNER JOIN field_data_field_killorder_num c ON c.entity_id=a.nid
            //                  INNER JOIN field_data_field_killorder_user d ON d.entity_id=a.nid
            //                  WHERE b.field_killorder_killactivity_nid='".$killactivityinfo->nid."' AND d.field_killorder_user_nid='".$uid."'")->fetchAll();
            $getnum=0;
            // foreach ($arrNum as $keynum => $valuenum) {
            //     $getnum=$getnum+$valuenum->field_killorder_num_value;
            // }
            $havebuy=db_query("SELECT num FROM warrior_kill_data_list WHERE uid='".$uid."' AND activitynid='".$activitynid."'")->fetchColumn();
            if($havebuy){
                $getnum=$havebuy;
            }

            if($userinfo->field_employeesinfo_level['und'][0]['value']=='初级'){
                $amount=$killactivityinfo->field_killactivity_amount['und'][0]['value'];
            }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='中级'){
                $amount=$killactivityinfo->field_killactivity_amountmedium['und'][0]['value'];
            }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='高级'){
                $amount=$killactivityinfo->field_killactivity_amounthigh['und'][0]['value'];
            }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='顶级'){
                $amount=$killactivityinfo->field_killactivity_amounttop['und'][0]['value'];
            }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='签约店'){
                $amount=$killactivityinfo->field_qian_yue_dian['und'][0]['value'];//

            }else{
                $amount=$killactivityinfo->field_killactivity_amount['und'][0]['value'];
            }

            if(($getnum+$num)>$amount){
                fclose($fp);
                //get表示已经获得的；num表示要买的；amount表示总的可以买的;
                result(2,'限购数量已上限');
            }

            flock($fp,LOCK_UN);

        }

        //总价格
        $killgoodstotalprice=$killgoodsinfo->field_killgoods_price['und'][0]['value']*$num;

        $node = new stdClass;
        $node->type = "killorder";
        $node->language = "und";
        $node->title = 'ms'.date('YmdHis',time()).rand(1000,9999);
        $node->uid = $uid;
        $node->field_killorder_user['und'][0]['nid'] = $uid;
        $node->field_killorder_killactivity['und'][0]['nid'] = $activitynid;
        $node->field_killorder_killgoods['und'][0]['nid'] = $killgoodsid;
        $node->field_killorder_num['und'][0]['value'] = $num;
        $node->field_killorder_totalprice['und'][0]['value'] = $killgoodstotalprice;
        $node->field_killorder_serialnum['und'][0]['value'] = 'ms'.date('YmdHis',time()).rand(1000,9999);
        $node->field_killorder_status['und'][0]['value'] = '已下单';
        node_save($node);

        $killid=db_query("SELECT killid FROM warrior_kill_data_list WHERE uid='".$uid."' AND activitynid='".$activitynid."'")->fetchColumn();
        if($killid){
            db_query("UPDATE warrior_kill_data_list SET num=num+'".$num."' WHERE killid='".$killid."'");
        }else{
            db_query("INSERT INTO warrior_kill_data_list VALUES (NULL,'".$uid."','".$activitynid."','".$num."')");
        }

        $killgoodsinfo->field_killgoods_inventory['und'][0]['value']=$killgoodsinfo->field_killgoods_inventory['und'][0]['value']-$num;
        $killgoodsinfo->field_killgoods_buynum['und'][0]['value']=$killgoodsinfo->field_killgoods_buynum['und'][0]['value']+$num;
        node_save($killgoodsinfo);
        fclose($fp);
        result(1,'购买成功');
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getkillgoodsorderlist(){
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    if($openid && $userid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        // $userinfo=node_load($uid);
        // $userinfo->field_employeesinfo_dotime['und'][0]['value']=date('Y-m-d H:i:s',time());
        // node_save($userinfo);
        $arrNid=db_query("SELECT a.nid FROM node a
                          INNER JOIN field_data_field_killorder_user b ON b.entity_id=a.nid
                          WHERE a.type='killorder' AND b.field_killorder_user_nid='".$uid."' ORDER BY a.created DESC")->fetchAll();
        $i=0;
        foreach ($arrNid as $key => $value) {
            $node=node_load($value->nid);
            $killgoodsinfo=node_load($node->field_killorder_killgoods['und'][0]['nid']);
            $arrList[$i]['killorderid']=$node->nid;
            $arrList[$i]['title']=$killgoodsinfo->title;
            $arrList[$i]['brand']=$killgoodsinfo->field_killgoods_brandtext['und'][0]['value'];
            $arrList[$i]['codeid']=$killgoodsinfo->field_killgoods_code['und'][0]['value'];
            $arrList[$i]['num']=$node->field_killorder_num['und'][0]['value'];
            $arrList[$i]['price']=$killgoodsinfo->field_killgoods_price['und'][0]['value'];
            $arrList[$i]['pic']=image_style_url('listimg', $killgoodsinfo->field_killgoods_pic['und'][0]['uri']);
            $arrList[$i]['totalprice']=$node->field_killorder_totalprice['und'][0]['value'];
            if($node->field_killorder_status['und'][0]['value']){
                $arrList[$i]['orderstatus']=$node->field_killorder_status['und'][0]['value'];
            }else{
                $arrList[$i]['orderstatus']='已下单';
            }
            $i++;
        }
        result(1,'成功',$arrList);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getkillgoodsorderinfo(){
    $killorderid=$_REQUEST['killorderid'];
    if($killorderid){
        $node=node_load($killorderid);
        $killgoodsinfo=node_load($node->field_killorder_killgoods['und'][0]['nid']);
        $orderinfo['title']=$killgoodsinfo->title;
        $orderinfo['brand']=$killgoodsinfo->field_killgoods_brandtext['und'][0]['value'];
        $orderinfo['codeid']=$killgoodsinfo->field_killgoods_code['und'][0]['value'];
        $orderinfo['num']=$node->field_killorder_num['und'][0]['value'];
        $orderinfo['price']=$killgoodsinfo->field_killgoods_price['und'][0]['value'];
        $orderinfo['pic']=image_style_url('listimg', $killgoodsinfo->field_killgoods_pic['und'][0]['uri']);
        $orderinfo['totalprice']=$node->field_killorder_totalprice['und'][0]['value'];
        $orderinfo['created']=date('Y-m-d H:i:s',$node->created);
        $orderinfo['paytype']='支付';
        $orderinfo['ordercodeid']=$node->title;
        $orderinfo['serialnum']=$node->field_killorder_serialnum['und'][0]['value'];
        $orderinfo['couriercompany']=$node->field_killorder_kdgs['und'][0]['value'];
        $orderinfo['couriernumber']=$node->field_killorder_kddh['und'][0]['value'];
        if($node->field_killorder_status['und'][0]['value']){
            $orderinfo['orderstatus']=$node->field_killorder_status['und'][0]['value'];
        }else{
            $orderinfo['orderstatus']='已下单';
        }
        result(1,'成功',$orderinfo);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getusername(){
    global $user;
    $output['name']=$user->name;
    result(1,'成功',$output);
}

function warrior_getuserid(){
    $nid=$_REQUEST['nid'];
    $node=node_load($nid);
    $output['employeesinfoid']=$node->field_employeesinfo_id['und'][0]['value'];
    result(1,'成功',$output);
}

function warrior_getopenid(){
    $code=$_REQUEST['code'];
    $options = array(
        'appid'=>'wxb540b3a16c9080b9',
        'appsecret'=>'076619a756b0c530f732f585b063b1b2'
    );
    if($code){
        $url="https://api.weixin.qq.com/sns/jscode2session?appid=".$options['appid']."&secret=".$options['appsecret']."&js_code=".$code."&grant_type=authorization_code";
        $result=httpGet($url);
        $arrWxInfo=json_decode($result);
        if(isset($arrWxInfo->openid)){
            $openid=$arrWxInfo->openid;
            $sessionkey=$arrWxInfo->session_key;
            $output['openid']=$openid;
            $output['sessionkey']=$sessionkey;
            result(1,'成功',$output);
        }else{
            result(40002,'无效code');
        }
    }else{
        result(40001,'缺少code参数');
    }
}

function warrior_getwxinfo(){
    $encryptedData=$_REQUEST['encryptedData'];
    $iv=urldecode($_REQUEST['iv']);
    $sessionkey=$_REQUEST['sessionkey'];
    $options = array(
        'appid'=>'wxb540b3a16c9080b9',
        'appsecret'=>'076619a756b0c530f732f585b063b1b2'
    );
    if($encryptedData && $iv && $sessionkey){
        include_once "wxBizDataCrypt.php";
        $pc = new WXBizDataCrypt($options['appid'], $sessionkey);
        $errCode = $pc->decryptData($encryptedData, $iv, $data);
        if ($errCode == 0) {
            $infodata=json_decode($data);
            // $output['unionid']=$infodata->unionId;
            $output['openid']=$infodata->openId;
            $output['nickname']=$infodata->nickName;
            $output['gender']=$infodata->gender;
            $output['headurl']=$infodata->avatarUrl;
            result(1,'获取成功',$output);
        }else{
            result(2,'获取失败',$errCode);
        }
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_login(){
    $userid=$_REQUEST['userid'];
    $mobile=$_REQUEST['mobile'];
    $openid=$_REQUEST['openid'];
    $nickname=base64_encode($_REQUEST['nickname']);
    $headurl=$_REQUEST['headurl'];
    $gender=$_REQUEST['gender'];
    if($userid && $mobile && $openid){
        //修改的部分2020-4-1

        if ((int)$userid == 14767) {
            if (strpos($mobile,'13669330606') !== false) {
                $mobile = '13669330606‬';
            }

        }


        // $id = db_query("SELECT a.nid FROM node a
        //                 INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
        //                 INNER JOIN field_data_field_employeesinfo_mobile c ON c.entity_id=a.nid
        //                 WHERE b.field_employeesinfo_id_value=".$userid." AND c.field_employeesinfo_mobile_value='".$mobile."'")->fetchColumn();
        // //修改的部分--end2020-4-1
        $id = db_query("SELECT a.nid FROM node a
                        INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                        INNER JOIN field_data_field_employeesinfo_mobile c ON c.entity_id=a.nid
                        WHERE b.field_employeesinfo_id_value='".$userid."' AND c.field_employeesinfo_mobile_value='".$mobile."'")->fetchColumn();

        if($id){
            // $bindid = db_query("SELECT a.nid FROM node a
            //             INNER JOIN field_data_field_employeesinfo_openid b ON b.entity_id=a.nid
            //             WHERE b.field_employeesinfo_openid_value='".$openid."'")->fetchColumn();
            // if($id!=$bindid){
            //     result(3,'账号与绑定微信号不符');
            // }
            $node=node_load($id);
            if(!$node->field_employeesinfo_openid['und'][0]['value'] || $openid==$node->field_employeesinfo_openid['und'][0]['value']){
                $node->field_employeesinfo_sex['und'][0]['value']=$gender;
                $node->field_employeesinfo_openid['und'][0]['value']=$openid;
                $node->field_employeesinfo_nickname['und'][0]['value']=$nickname;
                $node->field_employeesinfo_headurl['und'][0]['value']=$headurl;
                $node->field_employeesinfo_dotime['und'][0]['value']=date('Y-m-d H:i:s',time());
                node_save($node);
                result(1,'登陆成功');
            }else{
                result(3,'账号与绑定微信号不符');
            }
        }else{
            result(2,'用户不存在');
        }
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getkvlist(){
    $node=node_load(3);
    foreach ($node->field_kv_img['und'] as $key => $value) {
        $picinfo=field_collection_item_load($value['value']);
        $arrList[$key]['picurl']=image_style_url('kvimg', $picinfo->field_kv_pic['und'][0]['uri']);
        $arrList[$key]['link']=$picinfo->field_kv_link['und'][0]['value'];
    }
    result(1,'成功',$arrList);
}

function warrior_getindexintegralgoodslist(){
    $arrGoodsNid = db_query("SELECT nid FROM node
                             WHERE type='integralgoods' AND status=1 ORDER BY created DESC LIMIT 3")->fetchAll();
    $i=0;
    foreach ($arrGoodsNid as $key => $value) {
        $node=node_load($value->nid);
        $arrList[$i]['nid']=$node->nid;
        $arrList[$i]['title']=$node->title;
        $arrList[$i]['brand']=$node->field_integralgoods_brand['und'][0]['value'];
        $arrList[$i]['exchangeint']=$node->field_integralgoods_exchangeint['und'][0]['value'];
        $arrList[$i]['codeid']=$node->field_integralgoods_id['und'][0]['value'];
        $arrList[$i]['pic']=image_style_url('listimg', $node->field_integralgoods_pic['und'][0]['uri']);
        $i++;
    }
    result(1,'成功',$arrList);
}

function warrior_getindexpromptgoodslist(){
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
    // $userinfo=node_load($uid);
    // $userinfo->field_employeesinfo_dotime['und'][0]['value']=date('Y-m-d H:i:s',time());
    // node_save($userinfo);
    $nowtimedmyhis=date('Y-m-d H:i:s',time());
    $arrGoodsNid = db_query("SELECT a.nid FROM node a
                             INNER JOIN field_data_field_promptgoods_endtime b ON b.entity_id=a.nid
                             INNER JOIN field_data_field_promptgoods_brandbelong c ON c.entity_id=a.nid
                             WHERE a.type='promptgoods' AND a.status=1 AND b.field_promptgoods_endtime_value>'".$nowtimedmyhis."' AND c.field_promptgoods_brandbelong_value='".$userinfo->field_employeesinfo_brand['und'][0]['value']."' ORDER BY a.created DESC LIMIT 3")->fetchAll();
    $i=0;
    foreach ($arrGoodsNid as $key => $value) {
        $node=node_load($value->nid);
        if(time()>strtotime($node->field_promptgoods_starttime['und'][0]['value']) && time()<strtotime($node->field_promptgoods_endtime['und'][0]['value'])){
            $arrList[$i]['nid']=$node->nid;
            $arrList[$i]['title']=$node->title;
            $arrList[$i]['brand']=$node->field_promptgoods_brand['und'][0]['value'];
            $arrList[$i]['price']=$node->field_promptgoods_price['und'][0]['value'];
            $arrList[$i]['codeid']=$node->field_promptgoods_id['und'][0]['value'];
            $arrList[$i]['starttime']=strtotime($node->field_promptgoods_starttime['und'][0]['value'])*1000;
            $arrList[$i]['endtime']=strtotime($node->field_promptgoods_endtime['und'][0]['value'])*1000;
            $arrList[$i]['status']='进行中';
            $arrList[$i]['pic']=image_style_url('listimg', $node->field_promptgoods_pic['und'][0]['uri']);
            $i++;
        }
    }
    result(1,'成功',$arrList);
}

function warrior_getintegralgoodslist(){
    $page=$_REQUEST['page'];
    $goodstype=$_REQUEST['goodstype'];
    $conditions=$_REQUEST['conditions'];
    $interval=$_REQUEST['interval'];
    $keyword=$_REQUEST['keyword'];
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    // if($userid){
    //     $uid=db_query("SELECT a.nid FROM node a
    //                    INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
    //                    WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
    //     $userinfo=node_load($uid);
    //     $userinfo->field_employeesinfo_dotime['und'][0]['value']=date('Y-m-d H:i:s',time());
    //     node_save($userinfo);
    // }
    if($page){
        $page=($page-1)*10;
    }else{
        $page=0;
    }
    if($keyword){
        $titlesql=" AND title like '%".$keyword."%'";
        $atitlesql=" AND a.title like '%".$keyword."%'";
    }
    if($goodstype){
        $goodstypesql=" AND field_integralgoods_type_value='".$type."'";
        $bgoodstypesql=" AND b.field_integralgoods_type_value='".$type."'";
    }


    if($interval=='1'){
        $intervalinnerjoinsql = 'INNER JOIN field_data_field_integralgoods_exchangeint iii ON iii.entity_id=a.nid';
        $intervalsql = ' AND iii.field_integralgoods_exchangeint_value>0 AND iii.field_integralgoods_exchangeint_value<=500';
    }else if($interval=='2'){
        $intervalinnerjoinsql = 'INNER JOIN field_data_field_integralgoods_exchangeint iii ON iii.entity_id=a.nid';
        $intervalsql = ' AND iii.field_integralgoods_exchangeint_value>500 AND iii.field_integralgoods_exchangeint_value<=1000';
    }else if($interval=='3'){
        $intervalinnerjoinsql = 'INNER JOIN field_data_field_integralgoods_exchangeint iii ON iii.entity_id=a.nid';
        $intervalsql = ' AND iii.field_integralgoods_exchangeint_value>1000 AND iii.field_integralgoods_exchangeint_value<=2000';
    }else if($interval=='4'){
        $intervalinnerjoinsql = 'INNER JOIN field_data_field_integralgoods_exchangeint iii ON iii.entity_id=a.nid';
        $intervalsql = ' AND iii.field_integralgoods_exchangeint_value>2000 AND iii.field_integralgoods_exchangeint_value<=4000';
    }else if($interval=='5'){
        $intervalinnerjoinsql = 'INNER JOIN field_data_field_integralgoods_exchangeint iii ON iii.entity_id=a.nid';
        $intervalsql = ' AND iii.field_integralgoods_exchangeint_value>4000 AND iii.field_integralgoods_exchangeint_value<=8000';
    }else if($interval=='6'){
        $intervalinnerjoinsql = 'INNER JOIN field_data_field_integralgoods_exchangeint iii ON iii.entity_id=a.nid';
        $intervalsql = ' AND iii.field_integralgoods_exchangeint_value>8000';
    }else{
        $intervalinnerjoinsql = '';
        $intervalsql = '';
    }

    if($conditions=='all'){
        $arrGoodsNid = db_query("SELECT nid FROM node a ".$intervalinnerjoinsql."
                                 INNER JOIN field_data_field_field_integralgoods_asc w ON w.entity_id=a.nid
                                 WHERE a.type='integralgoods' AND a.status=1 ".$atitlesql.$intervalsql." ORDER BY w.field_field_integralgoods_asc_value ASC,a.changed DESC LIMIT $page,10")->fetchAll();
    }else if($conditions=='new'){
        $arrGoodsNid = db_query("SELECT nid FROM node a ".$intervalinnerjoinsql."
                                 INNER JOIN field_data_field_field_integralgoods_asc w ON w.entity_id=a.nid
                                 WHERE type='integralgoods' AND status=1 ".$titlesql.$intervalsql." ORDER BY w.field_field_integralgoods_asc_value ASC,a.changed DESC LIMIT $page,10")->fetchAll();
    }else if($conditions=='hot'){
        $arrGoodsNid = db_query("SELECT a.nid FROM node a ".$intervalinnerjoinsql."
                                 INNER JOIN field_data_field_integralgoods_exchangenum b ON b.entity_id=a.nid
                                 INNER JOIN field_data_field_field_integralgoods_asc w ON w.entity_id=a.nid
                                 WHERE a.type='integralgoods' AND a.status=1 ".$atitlesql.$intervalsql." ORDER BY b.field_integralgoods_exchangenum_value DESC,w.field_field_integralgoods_asc_value ASC LIMIT $page,10")->fetchAll();
    }else if($conditions=='recommend'){
        $arrGoodsNid = db_query("SELECT a.nid FROM node a ".$intervalinnerjoinsql."
                                 INNER JOIN field_data_field_integralgoods_recommend b ON b.entity_id=a.nid
                                 INNER JOIN field_data_field_field_integralgoods_asc w ON w.entity_id=a.nid
                                 WHERE a.type='integralgoods' AND a.status=1 ".$atitlesql.$intervalsql." ORDER BY w.field_field_integralgoods_asc_value ASC,a.changed DESC LIMIT $page,10")->fetchAll();
    }else if($conditions=='机油类' || $conditions=='鞋类' || $conditions=='肥皂类' || $conditions=='生活用品' || $conditions=='轮胎工具' || $conditions=='大型设备'){
        $arrGoodsNid = db_query("SELECT nid FROM node a ".$intervalinnerjoinsql."
                                 INNER JOIN field_data_field_integralgoods_type b ON b.entity_id=a.nid
                                 INNER JOIN field_data_field_field_integralgoods_asc w ON w.entity_id=a.nid
                                 WHERE type='integralgoods' AND status=1 ".$atitlesql.$intervalsql." AND b.field_integralgoods_type_value='".$conditions."' ORDER BY w.field_field_integralgoods_asc_value ASC,a.changed DESC LIMIT $page,10")->fetchAll();
    }else{
        $arrGoodsNid = db_query("SELECT nid FROM node a ".$intervalinnerjoinsql."
                                 INNER JOIN field_data_field_field_integralgoods_asc w ON w.entity_id=a.nid
                                 WHERE a.type='integralgoods' AND a.status=1 ".$atitlesql.$intervalsql." ORDER BY w.field_field_integralgoods_asc_value ASC,a.changed DESC LIMIT $page,10")->fetchAll();
    }
    $i=0;
    foreach ($arrGoodsNid as $key => $value) {
        $node=node_load($value->nid);
        $arrList[$i]['nid']=$node->nid;
        $arrList[$i]['title']=$node->title;
        $arrList[$i]['brand']=$node->field_integralgoods_brand['und'][0]['value'];
        $arrList[$i]['exchangeint']=$node->field_integralgoods_exchangeint['und'][0]['value'];
        $arrList[$i]['codeid']=$node->field_integralgoods_id['und'][0]['value'];
        $arrList[$i]['pic']=image_style_url('listimg', $node->field_integralgoods_pic['und'][0]['uri']);
        $i++;
    }
    result(1,'成功',$arrList);
}

function warrior_getintegralgoodsinfo(){
    $nid=$_REQUEST['nid'];
    if($nid){
        $node=node_load($nid);
        $info['nid']=$node->nid;
        $info['title']=$node->title;
        $info['brand']=$node->field_integralgoods_brand['und'][0]['value'];
        $info['exchangeint']=$node->field_integralgoods_exchangeint['und'][0]['value'];
        $info['codeid']=$node->field_integralgoods_id['und'][0]['value'];
        // $info['inventory']=$node->field_integralgoods_inventory['und'][0]['value'];
        // $info['amount']=$node->field_integralgoods_amount['und'][0]['value'];
        ##$info['introduce']=str_replace('src="', 'src="https://app.rteam.cn',$node->body['und'][0]['value']);
        $info['introduce']=str_replace('src="', 'src="https://warrior.beats-digital.com',$node->body['und'][0]['value']);
        foreach ($node->field_integralgoods_pic['und'] as $key => $value) {
            $info['arrpic'][$key]=image_style_url('detailpic', $value['uri']);
        }
        result(1,'成功',$info);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getpromptgoodslist(){
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    $page=$_REQUEST['page'];
    if(!$page){
        $page=0;
    }else{
        $page=($page-1)*100000;
    }
    $arrActivityNid=db_query("SELECT a.nid FROM node a
                              INNER JOIN field_data_field_promptactivity_enable b ON b.entity_id=a.nid
                              WHERE a.type='promptactivity' AND b.field_promptactivity_enable_value='是' LIMIT $page,100000")->fetchAll();
    $uid=db_query("SELECT a.nid FROM node a
                   INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                   WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
    $userinfo=node_load($uid);
    // $userinfo->field_employeesinfo_dotime['und'][0]['value']=date('Y-m-d H:i:s',time());
    // node_save($userinfo);
    foreach ($arrActivityNid as $key => $value) {
        $promptactivityinfo=node_load($value->nid);
        if(strtotime($promptactivityinfo->field_promptactivity_endtime['und'][0]['value'])>time() && strtotime($promptactivityinfo->field_promptactivity_starttime['und'][0]['value'])<time()){
            $i=0;
            foreach ($promptactivityinfo->field_promptactivity_promptgoods['und'] as $key1 => $value1) {
                $promptgoodsinfo=node_load($value1['nid']);
                // $promptgoodsinfo=json_decode(httpGet("http://app.rteam.cn/warriornew/?q=getqgspxx/".$value1['nid']));
                // var_dump($promptgoodsinfo);
                // echo $promptgoodsinfo->nodes[0]->node->brandbelong;die();
                if($userinfo->field_employeesinfo_brand['und'][0]['value']==$promptgoodsinfo->field_promptgoods_brandbelong['und'][0]['value']){
                    if($promptgoodsinfo->field_promptgoods_inventory['und'][0]['value']>0){
                        $arrlist[$i]['activitynid']=$promptactivityinfo->nid;
                        $arrlist[$i]['activityname']=$promptactivityinfo->title;
                        $arrlist[$i]['starttime']=strtotime($promptactivityinfo->field_promptactivity_starttime['und'][0]['value'])*1000;
                        $arrlist[$i]['endtime']=strtotime($promptactivityinfo->field_promptactivity_endtime['und'][0]['value'])*1000;
                        // $arrlist[$i]['nid']=$promptgoodsinfo->nodes[0]->node->nid;
                        // $arrlist[$i]['title']=$promptgoodsinfo->nodes[0]->node->title;
                        // $arrlist[$i]['brand']=$promptgoodsinfo->nodes[0]->node->brand;
                        // $arrlist[$i]['codeid']=$promptgoodsinfo->nodes[0]->node->codeid;
                        // $arrlist[$i]['introduce']=str_replace('src="', 'src="https://app.rteam.cn',$promptgoodsinfo->nodes[0]->node->introduce);
                        // // $arrlist[$i]['originalprice']=$promptgoodsinfo->field_promptgoods_originalprice['und'][0]['value'];
                        // $arrlist[$i]['price']=$promptgoodsinfo->nodes[0]->node->price;
                        $arrlist[$i]['nid']=$promptgoodsinfo->nid;
                        $arrlist[$i]['title']=$promptgoodsinfo->title;
                        $arrlist[$i]['brand']=$promptgoodsinfo->field_promptgoods_brand['und'][0]['value'];
                        $arrlist[$i]['codeid']=$promptgoodsinfo->field_promptgoods_id['und'][0]['value'];
                        //$arrlist[$i]['introduce']=str_replace('src="', 'src="https://alicdn.rteam.cn',$promptgoodsinfo->field_promptgoods_body['und'][0]['value']);
                        $arrlist[$i]['introduce']=str_replace('src="', 'src="https://warrior.beats-digital.com',$promptgoodsinfo->field_promptgoods_body['und'][0]['value']);
                        // $arrlist[$i]['originalprice']=$promptgoodsinfo->field_promptgoods_originalprice['und'][0]['value'];
                        $arrlist[$i]['price']=$promptgoodsinfo->field_promptgoods_price['und'][0]['value'];

                        // $arrNum=db_query("SELECT c.field_snapuporder_num_value FROM node a
                        //                  INNER JOIN field_data_field_snapuporder_activitynid b ON b.entity_id=a.nid
                        //                  INNER JOIN field_data_field_snapuporder_num c ON c.entity_id=a.nid
                        //                  INNER JOIN field_data_field_snapuporder_user d ON d.entity_id=a.nid
                        //                  WHERE b.field_snapuporder_activitynid_nid='".$promptactivityinfo->nid."' AND d.field_snapuporder_user_nid='".$userinfo->nid."'")->fetchAll();
                        // $num=0;
                        // foreach ($arrNum as $keynum => $valuenum) {
                        //     $num=$num+$valuenum->field_snapuporder_num_value;
                        // }
                        $arrlist[$i]['inventory']=$promptgoodsinfo->field_promptgoods_inventory['und'][0]['value'];
                        $amount=0;
                        if($userinfo->field_employeesinfo_level['und'][0]['value']=='初级'){
                            $amount=$promptactivityinfo->field_prompt_amount['und'][0]['value'];
                        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='中级'){
                            $amount=$promptactivityinfo->field_prompt_amountmedium['und'][0]['value'];
                        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='高级'){
                            $amount=$promptactivityinfo->field_prompt_amounthigh['und'][0]['value'];
                        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='顶级'){
                            $amount=$promptactivityinfo->field_prompt_amounttop['und'][0]['value'];
                        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='签约店'){
                            $amount=$promptactivityinfo->field_xiangou_huo['und'][0]['value'];
                        }else{
                            $amount=$promptactivityinfo->field_prompt_amount['und'][0]['value'];
                        }
                        $arrlist[$i]['amount']=$amount;
                        $arrlist[$i]['flprice']=$promptgoodsinfo->field_promptgoods_flprice['und'][0]['value'];

                        $num=0;
                        $havebuy=db_query("SELECT num FROM warrior_prompt_data_list WHERE uid='".$uid."' AND activitynid='".$value->nid."'")->fetchColumn();
                        if($havebuy){
                            $num=$havebuy;
                        }
                        $arrlist[$i]['amount']=$amount-$num;
                        // if($arrlist[$i]['amount']<0){
                        //     $arrlist[$i]['amount']=0;
                        // }
                        $arrlist[$i]['pic']=str_replace('src="', 'src="https://alicdn.rteam.cn',image_style_url('listimg', $promptgoodsinfo->field_promptgoods_pic['und'][0]['uri']));
                        // $arrlist[$i]['arrpic']=explode(", ",$promptgoodsinfo->nodes[0]->node->arrpic);
                        foreach ($promptgoodsinfo->field_promptgoods_pic['und'] as $keypic => $valuepic) {
                            $arrlist[$i]['arrpic'][]=str_replace('src="', 'src="https://alicdn.rteam.cn',image_style_url('detailpic', $valuepic['uri']));
                        }
                        $i++;
                    }
                }

            }
        }
    }
    result(1,'成功',$arrlist);
}



function warrior_getpromptgoodsinfo(){
    $nid=$_REQUEST['nid'];
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    $activitynid=$_REQUEST['activitynid'];

    if($nid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $userinfo=node_load($uid);
        // node_save($userinfo);
        $node=node_load($nid);
        $info['nid']=$node->nid;
        $info['title']=$node->title;
        $info['brand']=$node->field_promptgoods_brand['und'][0]['value'];
        $info['originalprice']=$node->field_promptgoods_originalprice['und'][0]['value'];
        $info['price']=$node->field_promptgoods_price['und'][0]['value'];
        $info['flprice']=$node->field_promptgoods_flprice['und'][0]['value'];
        $info['codeid']=$node->field_promptgoods_id['und'][0]['value'];

        $promptactivityinfo=node_load($activitynid);
        // $arrNum=db_query("SELECT c.field_snapuporder_num_value FROM node a
        //                  INNER JOIN field_data_field_snapuporder_activitynid b ON b.entity_id=a.nid
        //                  INNER JOIN field_data_field_snapuporder_num c ON c.entity_id=a.nid
        //                  INNER JOIN field_data_field_snapuporder_user d ON d.entity_id=a.nid
        //                  WHERE b.field_snapuporder_activitynid_nid='".$promptactivityinfo->nid."' AND d.field_snapuporder_user_nid='".$userinfo->nid."'")->fetchAll();
        $num=0;
        // foreach ($arrNum as $keynum => $valuenum) {
        //     $num=$num+$valuenum->field_snapuporder_num_value;
        // }
        $havebuy=db_query("SELECT num FROM warrior_prompt_data_list WHERE uid='".$uid."' AND activitynid='".$activitynid."'")->fetchColumn();
        if($havebuy){
            $num=$havebuy;
        }
        $info['inventory']=$node->field_promptgoods_inventory['und'][0]['value'];

        if($userinfo->field_employeesinfo_level['und'][0]['value']=='初级'){
            $amount=$promptactivityinfo->field_prompt_amount['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='中级'){
            $amount=$promptactivityinfo->field_prompt_amountmedium['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='高级'){
            $amount=$promptactivityinfo->field_prompt_amounthigh['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='顶级'){
            $amount=$promptactivityinfo->field_prompt_amounttop['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='签约店'){
            $amount=$promptactivityinfo->field_xiangou_huo['und'][0]['value'];//
        }else{
            $amount=$promptactivityinfo->field_prompt_amount['und'][0]['value'];
        }

        $info['flprice']=$node->field_promptgoods_flprice['und'][0]['value'];
        $info['amount']=$amount-$num;
        if($info['amount']<0){
            $info['amount']=0;
        }
        $info['inventory']=$node->field_promptgoods_inventory['und'][0]['value'];
        $info['activitynid']=$activitynid;
        $info['introduce']=str_replace('src="', 'src="https://app.rteam.cn',$node->field_promptgoods_body['und'][0]['value']);
        $info['starttime']=strtotime($promptactivityinfo->field_promptactivity_starttime['und'][0]['value'])*1000;
        $info['endtime']=strtotime($promptactivityinfo->field_promptactivity_endtime['und'][0]['value'])*1000;
        if(time()<strtotime($promptactivityinfo->field_promptactivity_starttime['und'][0]['value'])){
            $info['status']='未开始';
        }else if(time()>strtotime($promptactivityinfo->field_promptactivity_endtime['und'][0]['value'])){
            $info['status']='已结束';
        }else{
            $info['status']='进行中';
        }
        foreach ($node->field_promptgoods_pic['und'] as $key => $value) {
            $info['arrpic'][$key]=image_style_url('detailpic', $value['uri']);
        }
        result(1,'成功',$info);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getkillgoodslist(){
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    $page=$_REQUEST['page'];
    if(!$page){
        $page=0;
    }else{
        $page=($page-1)*100000;
    }
    $arrActivityNid=db_query("SELECT a.nid FROM node a
                              INNER JOIN field_data_field_killactivity_enable b ON b.entity_id=a.nid
                              WHERE a.type='killactivity' AND b.field_killactivity_enable_value='是' LIMIT $page,100000")->fetchAll();
    $uid=db_query("SELECT a.nid FROM node a
                   INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                   WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
    $userinfo=node_load($uid);
    // $userinfo->field_employeesinfo_dotime['und'][0]['value']=date('Y-m-d H:i:s',time());
    // node_save($userinfo);
    $arrlist=array();
    foreach ($arrActivityNid as $key => $value) {
        $killactivityinfo=node_load($value->nid);
        if(strtotime($killactivityinfo->field_killactivity_endtime['und'][0]['value'])>time() && strtotime($killactivityinfo->field_killactivity_starttime['und'][0]['value'])<time()){
            $i=0;
            foreach ($killactivityinfo->field_killactivity_killgoods['und'] as $key1 => $value1) {
                $killgoodsinfo=node_load($value1['nid']);
                // $killgoodsinfo=json_decode(httpGet("http://app.rteam.cn/warriornew/?q=getmsspxx/".$value1['nid']));
                // var_dump($killgoodsinfo);die();
                if($userinfo->field_employeesinfo_brand['und'][0]['value']==$killgoodsinfo->field_killgoods_brand['und'][0]['value']){
                    if($killgoodsinfo->field_killgoods_inventory['und'][0]['value']>0){
                        $arrlist[$i]['activitynid']=$killactivityinfo->nid;
                        $arrlist[$i]['activityname']=$killactivityinfo->title;
                        $arrlist[$i]['starttime']=strtotime($killactivityinfo->field_killactivity_starttime['und'][0]['value'])*1000;
                        $arrlist[$i]['endtime']=strtotime($killactivityinfo->field_killactivity_endtime['und'][0]['value'])*1000;
                        // $arrlist[$i]['killgoodsid']=$killgoodsinfo->nodes[0]->node->nid;
                        // $arrlist[$i]['title']=$killgoodsinfo->nodes[0]->node->title;
                        // $arrlist[$i]['brand']=$killgoodsinfo->nodes[0]->node->brand;
                        // $arrlist[$i]['codeid']=$killgoodsinfo->nodes[0]->node->codeid;
                        // $arrlist[$i]['introduce']=str_replace('src="', 'src="https://app.rteam.cn',$killgoodsinfo->nodes[0]->node->introduce);
                        // $arrlist[$i]['price']=$killgoodsinfo->nodes[0]->node->price;
                        // $arrlist[$i]['originalprice']=$killgoodsinfo->nodes[0]->node->originalprice;
                        // // $arrNum=db_query("SELECT c.field_killorder_num_value FROM node a
                        // //                  INNER JOIN field_data_field_killorder_killactivity b ON b.entity_id=a.nid
                        // //                  INNER JOIN field_data_field_killorder_num c ON c.entity_id=a.nid
                        // //                  INNER JOIN field_data_field_killorder_user d ON d.entity_id=a.nid
                        // //                  WHERE b.field_killorder_killactivity_nid='".$killactivityinfo->nid."' AND d.field_killorder_user_nid='".$userinfo->nid."'")->fetchAll();
                        // // $num=0;
                        // // foreach ($arrNum as $keynum => $valuenum) {
                        // //     $num=$num+$valuenum->field_killorder_num_value;
                        // // }
                        // $arrlist[$i]['inventory']=$killgoodsinfo->nodes[0]->node->inventory;
                        $arrlist[$i]['killgoodsid']=$killgoodsinfo->nid;
                        $arrlist[$i]['title']=$killgoodsinfo->title;
                        $arrlist[$i]['brand']=$killgoodsinfo->field_killgoods_brandtext['und'][0]['value'];
                        $arrlist[$i]['codeid']=$killgoodsinfo->field_killgoods_code['und'][0]['value'];
                        $arrlist[$i]['introduce']=str_replace('src="', 'src="https://alicdn.rteam.cn',$killgoodsinfo->field_killgoods_content['und'][0]['value']);
                        $arrlist[$i]['price']=$killgoodsinfo->field_killgoods_price['und'][0]['value'];
                        $arrlist[$i]['originalprice']=$killgoodsinfo->field_killgoods_originalprice['und'][0]['value'];
                        $arrlist[$i]['inventory']=$killgoodsinfo->field_killgoods_inventory['und'][0]['value'];
                        $amount=0;
                        if($userinfo->field_employeesinfo_level['und'][0]['value']=='初级'){
                            $amount=$killactivityinfo->field_killactivity_amount['und'][0]['value'];
                        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='中级'){
                            $amount=$killactivityinfo->field_killactivity_amountmedium['und'][0]['value'];
                        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='高级'){
                            $amount=$killactivityinfo->field_killactivity_amounthigh['und'][0]['value'];
                        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='顶级'){
                            $amount=$killactivityinfo->field_killactivity_amounttop['und'][0]['value'];
                        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='签约店'){
                            $amount=$killactivityinfo->field_qian_yue_dian['und'][0]['value'];//

                        }else{
                            $amount=$killactivityinfo->field_killactivity_amount['und'][0]['value'];
                        }
                        $arrlist[$i]['amount']=$amount;

                        $havebuy=db_query("SELECT num FROM warrior_kill_data_list WHERE uid='".$uid."' AND activitynid='".$value->nid."'")->fetchColumn();
                        $num=0;
                        if($havebuy){
                            $num=$havebuy;
                        }
                        $arrlist[$i]['amount']=$amount-$num;
                        if($arrlist[$i]['amount']<0){
                            $arrlist[$i]['amount']=0;
                        }
                        // $arrlist[$i]['pic']=$killgoodsinfo->nodes[0]->node->pic;
                        // $arrlist[$i]['arrpic']=explode(", ",$killgoodsinfo->nodes[0]->node->arrpic);
                        $arrlist[$i]['pic']=str_replace('src="', 'src="https://alicdn.rteam.cn',image_style_url('listimg', $killgoodsinfo->field_killgoods_pic['und'][0]['uri']));
                        // $arrlist[$i]['arrpic']=explode(", ",$promptgoodsinfo->nodes[0]->node->arrpic);
                        foreach ($killgoodsinfo->field_killgoods_pic['und'] as $keypic => $valuepic) {
                            $arrlist[$i]['arrpic'][]=str_replace('src="', 'src="https://alicdn.rteam.cn',image_style_url('detailpic', $valuepic['uri']));
                        }
                        $i++;
                    }
                }
            }
        }
    }
    result(1,'成功',$arrlist);
}

function warrior_getkillgoodsinfo(){
    $activitynid=$_REQUEST['activitynid'];
    $killgoodsid=$_REQUEST['killgoodsid'];
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    if($activitynid && $killgoodsid && $openid && $userid){
        $uid=db_query("SELECT a.nid FROM node a
                   INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                   WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $userinfo=node_load($uid);

        $killactivityinfo=node_load($activitynid);
        $killgoodsinfo=node_load($killgoodsid);

        $killgoods['activitynid']=$killactivityinfo->nid;
        $killgoods['activityname']=$killactivityinfo->title;
        $killgoods['starttime']=strtotime($killactivityinfo->field_killactivity_starttime['und'][0]['value'])*1000;
        $killgoods['endtime']=strtotime($killactivityinfo->field_killactivity_endtime['und'][0]['value'])*1000;

        $killgoods['killgoodsid']=$killgoodsinfo->nid;
        $killgoods['title']=$killgoodsinfo->title;
        $killgoods['brand']=$killgoodsinfo->field_killgoods_brandtext['und'][0]['value'];
        $killgoods['codeid']=$killgoodsinfo->field_killgoods_code['und'][0]['value'];
        $killgoods['introduce']=str_replace('src="', 'src="https://app.rteam.cn',$killgoodsinfo->field_killgoods_content['und'][0]['value']);
        $killgoods['originalprice']=$killgoodsinfo->field_killgoods_originalprice['und'][0]['value'];
        $killgoods['price']=$killgoodsinfo->field_killgoods_price['und'][0]['value'];

        // $arrNum=db_query("SELECT c.field_killorder_num_value FROM node a
        //                  INNER JOIN field_data_field_killorder_killactivity b ON b.entity_id=a.nid
        //                  INNER JOIN field_data_field_killorder_num c ON c.entity_id=a.nid
        //                  INNER JOIN field_data_field_killorder_user d ON d.entity_id=a.nid
        //                  WHERE b.field_killorder_killactivity_nid='".$killactivityinfo->nid."' AND d.field_killorder_user_nid='".$userinfo->nid."'")->fetchAll();
        $num=0;
        // foreach ($arrNum as $keynum => $valuenum) {
        //     $num=$num+$valuenum->field_killorder_num_value;
        // }
        $havebuy=db_query("SELECT num FROM warrior_kill_data_list WHERE uid='".$uid."' AND activitynid='".$activitynid."'")->fetchColumn();
        if($havebuy){
            $num=$havebuy;
        }
        if($userinfo->field_employeesinfo_level['und'][0]['value']=='初级'){
            $amount=$killactivityinfo->field_killactivity_amount['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='中级'){
            $amount=$killactivityinfo->field_killactivity_amountmedium['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='高级'){
            $amount=$killactivityinfo->field_killactivity_amounthigh['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='顶级'){
            $amount=$killactivityinfo->field_killactivity_amounttop['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='签约店'){
            $amount=$killactivityinfo->field_qian_yue_dian['und'][0]['value'];//

        }else{
            $amount=$killactivityinfo->field_killactivity_amount['und'][0]['value'];
        }

        $killgoods['amount']=$amount-$num;
        if($killgoods['amount']<0){
            $killgoods['amount']=0;
        }
        $killgoods['inventory']=$killgoodsinfo->field_killgoods_inventory['und'][0]['value'];
        foreach ($killgoodsinfo->field_killgoods_pic['und'] as $key => $value) {
            $killgoods['arrpic'][$key]=image_style_url('detailpic', $value['uri']);
        }
        result(1,'成功',$killgoods);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getuserinfo(){
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    if($userid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $node=node_load($uid);
        $node->field_employeesinfo_dotime['und'][0]['value']=date('Y-m-d H:i:s',time());
        node_save($node);
        $userinfo['uid']=$node->nid;
        $userinfo['usertype']=$node->field_employeesinfo_type['und'][0]['value'];
        $userinfo['brand']=$node->field_employeesinfo_brand['und'][0]['value'];
        $userinfo['name']=$node->title;
        $userinfo['userid']=$node->field_employeesinfo_id['und'][0]['value'];
        if(!$node->field_employeesinfo_wholesaler['und'][0]['value']){
            $userinfo['wholesaler']='';
        }else{
            $userinfo['wholesaler']=$node->field_employeesinfo_wholesaler['und'][0]['value'];
        }
        $userinfo['wholesalerid']=$node->field_employeesinfo_wholesalerid['und'][0]['value'];
        $userinfo['sex']=$node->field_employeesinfo_sex['und'][0]['value'];
        $userinfo['openid']=$node->field_employeesinfo_openid['und'][0]['value'];
        $userinfo['nickname']=base64_decode($node->field_employeesinfo_nickname['und'][0]['value']);
        $userinfo['headurl']=$node->field_employeesinfo_headurl['und'][0]['value'];
        if(!$node->field_employeesinfo_integral['und'][0]['value']){
            $userinfo['integral']=0;
        }else{
            $userinfo['integral']=$node->field_employeesinfo_integral['und'][0]['value'];
        }
        $userinfo['rebate']=$node->field_employeesinfo_rebate['und'][0]['value'];

        $userinfo['level']=$node->field_employeesinfo_brand['und'][0]['value'].$node->field_employeesinfo_level['und'][0]['value'].'客户';
        result(1,'成功',$userinfo);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getapplicationstatus(){
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    if($userid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $userinfo=node_load($uid);
        $status=db_query("SELECT c.field_application_status_value FROM node a
                          INNER JOIN field_data_field_application_lssid b ON b.entity_id=a.nid
                          INNER JOIN field_data_field_application_status c ON c.entity_id=a.nid
                          WHERE a.type='application' AND b.field_application_lssid_value='".$userinfo->field_employeesinfo_id['und'][0]['value']."'")->fetchColumn();
        if($status){
            result(1,'成功',$status);
        }else{
            result(2,'未申请');
        }
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_submitapplication(){
    $openid=$_REQUEST['openid'];
    $store=$_REQUEST['store'];
    $name=$_REQUEST['name'];
    $mobile=$_REQUEST['mobile'];
    $province=$_REQUEST['province'];
    $city=$_REQUEST['city'];
    $area=$_REQUEST['area'];
    $address=$_REQUEST['address'];
    $userid=$_REQUEST['userid'];//新增
    if($openid && $store && $name && $mobile && $province && $city && $area && $address && $uid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $node = new stdClass;
        $node->type = "application";
        $node->language = "und";
        $node->title = '服务申请';
        $node->uid = $uid;
        $node->field_application_status['und'][0]['value'] = '提交申请';
        $node->field_application_user['und'][0]['nid'] = $uid;
        $node->field_application_store['und'][0]['value'] = $store;
        $node->field_application_name['und'][0]['value'] = $name;
        $node->field_application_mobile['und'][0]['value'] = $mobile;
        $node->field_application_province['und'][0]['value'] = $province;
        $node->field_application_city['und'][0]['value'] = $city;
        $node->field_application_area['und'][0]['value'] = $area;
        $node->field_application_address['und'][0]['value'] = $address;
        node_save($node);
        result(1,'成功');
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_submitintegralgoodsorder(){
    $openid=$_REQUEST['openid'];
    $nid=$_REQUEST['nid'];
    $num=$_REQUEST['num'];
    $name=$_REQUEST['name'];
    $mobile=$_REQUEST['mobile'];
    $province=$_REQUEST['province'];
    $city=$_REQUEST['city'];
    $area=$_REQUEST['area'];
    $address=$_REQUEST['address'];
    $userid=$_REQUEST['userid'];//新增
    $remark=$_REQUEST['remark'];//备注
    if($openid && $nid && $num && $name && $mobile && $province && $city && $area && $address && $userid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $userinfo=node_load($uid);
        $goods=node_load($nid);
        $goodstotalintegral=$goods->field_integralgoods_exchangeint['und'][0]['value']*$num;
        // if($goods->field_integralgoods_inventory['und'][0]['value']<$num){
        //     result(3,'库存不足');
        // }
        // $userexchangenum=db_query("SELECT sum(b.field_exchangeorder_num_value) FROM field_data_field_exchangeorder_goods a
        //                            INNER JOIN field_data_field_exchangeorder_num b ON b.entity_id=a.entity_id
        //                            INNER JOIN field_data_field_exchangeorder_user c ON c.entity_id=a.entity_id
        //                            WHERE a.field_exchangeorder_goods_nid='".$nid."' AND c.field_exchangeorder_user_nid='".$uid."'")->fetchColumn();
        // if(!$userexchangenum){
        //     $userexchangenum=0;
        // }
        // if(($userexchangenum+$num)>$goods->field_integralgoods_amount['und'][0]['value']){
        //     result(4,'兑换数量超限');
        // }
        if($userinfo->field_employeesinfo_integral['und'][0]['value']>=$goodstotalintegral){
            $node = new stdClass;
            $node->type = "exchangeorder";
            $node->language = "und";
            $node->title = 'hl'.date('YmdHis',time()).rand(1000,9999);
            $node->uid = $uid;
            $node->field_exchangeorder_user['und'][0]['nid'] = $uid;
            $node->field_exchangeorder_goods['und'][0]['nid'] = $nid;
            $node->field_exchangeorder_num['und'][0]['value'] = $num;
            $node->field_exchangeorder_integral['und'][0]['value'] = $goodstotalintegral;
            $node->field_exchangeorder_totalprice['und'][0]['value'] = $goods->field_integralgoods_price['und'][0]['value']*$num;
            $node->field_exchangeorder_name['und'][0]['value'] = $name;
            $node->field_exchangeorder_mobile['und'][0]['value'] = $mobile;
            $node->field_exchangeorder_province['und'][0]['value'] = $province;
            $node->field_exchangeorder_city['und'][0]['value'] = $city;
            $node->field_exchangeorder_area['und'][0]['value'] = $area;
            $node->field_exchangeorder_address['und'][0]['value'] = $address;
            $node->field_exchangeorder_serialnum['und'][0]['value'] = 'hl'.date('YmdHis',time()).rand(1000,9999);
            $node->field_exchangeorder_status['und'][0]['value'] = '已下单';
            $node->field_exchangeorder_remark['und'][0]['value'] = $remark;
            node_save($node);

            $userinfo->field_employeesinfo_integral['und'][0]['value']=$userinfo->field_employeesinfo_integral['und'][0]['value']-$goodstotalintegral;
            node_save($userinfo);
            // $goods->field_integralgoods_inventory['und'][0]['value']=$goods->field_integralgoods_inventory['und'][0]['value']-$num;
            $goods->field_integralgoods_exchangenum['und'][0]['value']=$goods->field_integralgoods_exchangenum['und'][0]['value']+$num;
            node_save($goods);
            $output['integral']=$userinfo->field_employeesinfo_integral['und'][0]['value'];
            db_query("INSERT INTO warrior_user_integral_list VALUES (NULL,'礼品兑换','".$userinfo->field_employeesinfo_id['und'][0]['value']."','".$userinfo->title."','".(-$goodstotalintegral)."','".time()."','".$userinfo->field_employeesinfo_integral['und'][0]['value']."')");
            result(1,'兑换成功',$output);
        }else{
            result(2,'用户积分不足');
        }
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_submitpromptgoodsorder(){
    $openid=$_REQUEST['openid'];
    $nid=$_REQUEST['nid'];
    $num=$_REQUEST['num'];
    $userid=$_REQUEST['userid'];//新增
    $activitynid=$_REQUEST['activitynid'];
    // $name=$_REQUEST['name'];
    // $mobile=$_REQUEST['mobile'];
    // $province=$_REQUEST['province'];
    // $city=$_REQUEST['city'];
    // $area=$_REQUEST['area'];
    // $address=$_REQUEST['address'];
    if($openid && $nid && $num && $userid && $activitynid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $userinfo=node_load($uid);
        if($userinfo->field_employeesinfo_type['und'][0]['value']=='批发商'){
            result(5,'批发商不能参与抢购');
        }
        $goods=node_load($nid);
        $goodstotalprice=$goods->field_promptgoods_price['und'][0]['value']*$num;
        if($goods->field_promptgoods_inventory['und'][0]['value']<$num){
            result(3,'库存不足');
        }
        // $arrNum=db_query("SELECT c.field_snapuporder_num_value FROM node a
        //                  INNER JOIN field_data_field_snapuporder_activitynid b ON b.entity_id=a.nid
        //                  INNER JOIN field_data_field_snapuporder_num c ON c.entity_id=a.nid
        //                  INNER JOIN field_data_field_snapuporder_user d ON d.entity_id=a.nid
        //                  WHERE b.field_snapuporder_activitynid_nid='".$activitynid."' AND d.field_snapuporder_user_nid='".$userinfo->nid."'")->fetchAll();
        $promptactivityinfo=node_load($activitynid);
        if($userinfo->field_employeesinfo_level['und'][0]['value']=='初级'){
            $amount=$promptactivityinfo->field_prompt_amount['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='中级'){
            $amount=$promptactivityinfo->field_prompt_amountmedium['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='高级'){
            $amount=$promptactivityinfo->field_prompt_amounthigh['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='顶级'){
            $amount=$promptactivityinfo->field_prompt_amounttop['und'][0]['value'];
        }else if($userinfo->field_employeesinfo_level['und'][0]['value']=='签约店'){
            $amount=$promptactivityinfo->field_xiangou_huo['und'][0]['value']; //促销限购
        }else{
            if($userinfo->field_employeesinfo_brand['und'][0]['value']=='回力'){
                result(3,'你的等级不足');
            }else{
                $amount=$promptactivityinfo->field_prompt_amount['und'][0]['value'];
            }
        }

        $ygnum=0;
        // foreach ($arrNum as $keynum => $valuenum) {
        //     $ygnum=$ygnum+$valuenum->field_snapuporder_num_value;
        // }
        $havebuy=db_query("SELECT num FROM warrior_prompt_data_list WHERE uid='".$uid."' AND activitynid='".$activitynid."'")->fetchColumn();
        if($havebuy){
            $ygnum=$havebuy;
        }

        if(($ygnum+$num)>$amount){
            result(4,'兑换数量超限');
        }
        $node = new stdClass;
        $node->type = "snapuporder";
        $node->language = "und";
        $node->title = 'qg'.date('YmdHis',time()).rand(1000,9999);
        $node->uid = $uid;
        $node->field_snapuporder_user['und'][0]['nid'] = $uid;
        $node->field_snapuporder_activitynid['und'][0]['nid'] = $activitynid;
        $node->field_snapuporder_goods['und'][0]['nid'] = $nid;
        $node->field_snapuporder_num['und'][0]['value'] = $num;
        $node->field_snapuporder_totalprice['und'][0]['value'] = $goodstotalprice;
        $node->field_snapuporder_totalflprice['und'][0]['value'] = $goods->field_promptgoods_flprice['und'][0]['value']*$num;
        // $node->field_snapuporder_name['und'][0]['value'] = $name;
        // $node->field_snapuporder_mobile['und'][0]['value'] = $mobile;
        // $node->field_snapuporder_province['und'][0]['value'] = $province;
        // $node->field_snapuporder_city['und'][0]['value'] = $city;
        // $node->field_snapuporder_area['und'][0]['value'] = $area;
        // $node->field_snapuporder_address['und'][0]['value'] = $address;
        $node->field_snapuporder_serialnum['und'][0]['value'] = 'qg'.date('YmdHis',time()).rand(1000,9999);
        $node->field_snapuporder_status['und'][0]['value'] = '已下单';
        node_save($node);
        $promptid=db_query("SELECT promptid FROM warrior_prompt_data_list WHERE uid='".$uid."' AND activitynid='".$activitynid."'")->fetchColumn();
        if($promptid){
            db_query("UPDATE warrior_prompt_data_list SET num=num+'".$num."' WHERE promptid='".$promptid."'");
        }else{
            db_query("INSERT INTO warrior_prompt_data_list VALUES (NULL,'".$uid."','".$activitynid."','".$num."')");
        }
        // $goods->field_promptgoods_inventory['und'][0]['value']=$goods->field_promptgoods_inventory['und'][0]['value']-$num;
        $goods->field_promptgoods_buynum['und'][0]['value']=$goods->field_promptgoods_buynum['und'][0]['value']+$num;
        node_save($goods);
        result(1,'购买成功',$output);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_node_delete($node) {
    global $user;
    //print_r($node->type);die();
    //print_r($user->uid);die();
    if($node->type=='snapuporder'){
        //print_r($node);die();
        db_query("UPDATE warrior_prompt_data_list SET num=num-'".$node->field_snapuporder_num['und'][0]['value']."' WHERE uid='".$node->field_snapuporder_user['und'][0]['nid']."' AND activitynid='".$node->field_snapuporder_activitynid['und'][0]['nid']."'");
    }
}



function warrior_getintegralgoodsorderlist(){
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    if($openid && $userid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $arrNid=db_query("SELECT a.nid FROM node a
                          INNER JOIN field_data_field_exchangeorder_user b ON b.entity_id=a.nid
                          WHERE a.type='exchangeorder' AND b.field_exchangeorder_user_nid='".$uid."' ORDER BY a.created DESC")->fetchAll();
        $i=0;
        foreach ($arrNid as $key => $value) {
            $node=node_load($value->nid);
            $goods=node_load($node->field_exchangeorder_goods['und'][0]['nid']);
            $arrList[$i]['orderid']=$node->nid;
            $arrList[$i]['title']=$goods->title;
            $arrList[$i]['brand']=$goods->field_integralgoods_brand['und'][0]['value'];
            $arrList[$i]['codeid']=$goods->field_integralgoods_id['und'][0]['value'];
            $arrList[$i]['num']=$node->field_exchangeorder_num['und'][0]['value'];
            $arrList[$i]['integral']=$goods->field_integralgoods_exchangeint['und'][0]['value'];
            $arrList[$i]['pic']=image_style_url('listimg', $goods->field_integralgoods_pic['und'][0]['uri']);
            $arrList[$i]['totalnum']=$node->field_exchangeorder_num['und'][0]['value'];
            $arrList[$i]['totalintegral']=$node->field_exchangeorder_integral['und'][0]['value'];
            if($node->field_exchangeorder_status['und'][0]['value']){
                $arrList[$i]['orderstatus']=$node->field_exchangeorder_status['und'][0]['value'];
            }else{
                $arrList[$i]['orderstatus']='已下单';
            }
            $i++;
        }
        result(1,'成功',$arrList);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_cancelintegralgoodsorder(){
    $openid=$_REQUEST['openid'];
    $orderid=$_REQUEST['orderid'];
    $userid=$_REQUEST['userid'];//新增
    if($openid && $orderid && $userid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $userinfo=node_load($uid);
        $node=node_load($orderid);
        $node->field_snapuporder_status['und'][0]['value'] = '已取消';
        node_save($node);
        $userinfo->field_employeesinfo_integral['und'][0]['value']=$userinfo->field_employeesinfo_integral['und'][0]['value']+$node->field_exchangeorder_integral['und'][0]['value'];
        node_save($userinfo);
        db_query("INSERT INTO warrior_user_integral_list VALUES (NULL,'订单取消','".$userinfo->field_employeesinfo_id['und'][0]['value']."','".$userinfo->title."','".$node->field_exchangeorder_integral['und'][0]['value']."','".time()."','".$userinfo->field_employeesinfo_integral['und'][0]['value']."')");
        result(1,'成功');
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getpromptgoodsorderlist(){
    $openid=$_REQUEST['openid'];
    $userid=$_REQUEST['userid'];//新增
    if($openid && $userid){
        $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
        $arrNid=db_query("SELECT a.nid FROM node a
                          INNER JOIN field_data_field_snapuporder_user b ON b.entity_id=a.nid
                          WHERE a.type='snapuporder' AND b.field_snapuporder_user_nid='".$uid."' ORDER BY a.created DESC")->fetchAll();
        $i=0;
        foreach ($arrNid as $key => $value) {
            $node=node_load($value->nid);
            $goods=node_load($node->field_snapuporder_goods['und'][0]['nid']);
            $arrList[$i]['orderid']=$node->nid;
            $arrList[$i]['title']=$goods->title;
            $arrList[$i]['brand']=$goods->field_promptgoods_brand['und'][0]['value'];
            $arrList[$i]['codeid']=$goods->field_promptgoods_id['und'][0]['value'];
            $arrList[$i]['num']=$node->field_snapuporder_num['und'][0]['value'];
            $arrList[$i]['price']=$goods->field_promptgoods_price['und'][0]['value'];
            $arrList[$i]['pic']=image_style_url('listimg', $goods->field_promptgoods_pic['und'][0]['uri']);
            $arrList[$i]['totalnum']=$node->field_snapuporder_num['und'][0]['value'];
            $arrList[$i]['flprice']=$node->field_snapuporder_totalflprice['und'][0]['value']/$node->field_snapuporder_num['und'][0]['value'];
            $arrList[$i]['totalflprice']=$node->field_snapuporder_totalflprice['und'][0]['value'];
            $arrList[$i]['totalintegral']=$node->field_snapuporder_totalprice['und'][0]['value'];
            if($node->field_snapuporder_status['und'][0]['value']){
                $arrList[$i]['orderstatus']=$node->field_snapuporder_status['und'][0]['value'];
            }else{
                $arrList[$i]['orderstatus']='已下单';
            }
            $i++;
        }
        result(1,'成功',$arrList);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getintegralgoodsorderinfo(){
    $orderid=$_REQUEST['orderid'];
    if($orderid){
        $node=node_load($orderid);
        $orderinfo['name']=$node->field_exchangeorder_name['und'][0]['value'];
        $orderinfo['mobile']=$node->field_exchangeorder_mobile['und'][0]['value'];
        $orderinfo['address']=$node->field_exchangeorder_province['und'][0]['value'].$node->field_exchangeorder_city['und'][0]['value'].$node->field_exchangeorder_area['und'][0]['value'].$node->field_exchangeorder_address['und'][0]['value'];
        $goods=node_load($node->field_exchangeorder_goods['und'][0]['nid']);
        $orderinfo['title']=$goods->title;
        $orderinfo['brand']=$goods->field_integralgoods_brand['und'][0]['value'];
        $orderinfo['codeid']=$goods->field_integralgoods_id['und'][0]['value'];
        $orderinfo['num']=$node->field_exchangeorder_num['und'][0]['value'];
        $orderinfo['integral']=$goods->field_integralgoods_exchangeint['und'][0]['value'];
        $orderinfo['pic']=image_style_url('listimg', $goods->field_integralgoods_pic['und'][0]['uri']);
        $orderinfo['totalnum']=$node->field_exchangeorder_num['und'][0]['value'];
        $orderinfo['totalintegral']=$node->field_exchangeorder_integral['und'][0]['value'];
        $orderinfo['created']=date('Y-m-d H:i:s',$node->created);
        $orderinfo['paytype']='积分抵扣';
        $orderinfo['ordercodeid']=$node->title;
        $orderinfo['serialnum']=$node->field_exchangeorder_serialnum['und'][0]['value'];
        $orderinfo['couriercompany']=$node->field_exchangeorder_kdgs['und'][0]['value'];
        $orderinfo['couriernumber']=$node->field_exchangeorder_kddh['und'][0]['value'];
        if($node->field_exchangeorder_status['und'][0]['value']){
            $orderinfo['orderstatus']=$node->field_exchangeorder_status['und'][0]['value'];
        }else{
            $orderinfo['orderstatus']='已下单';
        }
        result(1,'成功',$orderinfo);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getpromptgoodsorderinfo(){
    $orderid=$_REQUEST['orderid'];
    if($orderid){
        $node=node_load($orderid);
        $orderinfo['name']=$node->field_snapuporder_name['und'][0]['value'];
        $orderinfo['mobile']=$node->field_snapuporder_mobile['und'][0]['value'];
        $orderinfo['address']=$node->field_snapuporder_province['und'][0]['value'].$node->field_snapuporder_city['und'][0]['value'].$node->field_snapuporder_area['und'][0]['value'].$node->field_snapuporder_address['und'][0]['value'];
        $goods=node_load($node->field_snapuporder_goods['und'][0]['nid']);
        $orderinfo['title']=$goods->title;
        $orderinfo['brand']=$goods->field_promptgoods_brand['und'][0]['value'];
        $orderinfo['codeid']=$goods->field_promptgoods_id['und'][0]['value'];
        $orderinfo['num']=$node->field_snapuporder_num['und'][0]['value'];
        $orderinfo['price']=$goods->field_promptgoods_price['und'][0]['value'];
        $orderinfo['pic']=image_style_url('listimg', $goods->field_promptgoods_pic['und'][0]['uri']);
        $orderinfo['flprice']=$node->field_snapuporder_totalflprice['und'][0]['value']/$node->field_snapuporder_num['und'][0]['value'];
        $orderinfo['totalflprice']=$node->field_snapuporder_totalflprice['und'][0]['value'];
        $orderinfo['totalnum']=$node->field_snapuporder_num['und'][0]['value'];
        $orderinfo['totalprice']=$node->field_snapuporder_totalprice['und'][0]['value'];
        $orderinfo['created']=date('Y-m-d H:i:s',$node->created);
        $orderinfo['paytype']='支付';
        $orderinfo['ordercodeid']=$node->title;
        $orderinfo['serialnum']=$node->field_snapuporder_serialnum['und'][0]['value'];
        $orderinfo['couriercompany']=$node->field_snapuporder_kdgs['und'][0]['value'];
        $orderinfo['couriernumber']=$node->field_snapuporder_kddh['und'][0]['value'];
        if($node->field_snapuporder_status['und'][0]['value']){
            $orderinfo['orderstatus']=$node->field_snapuporder_status['und'][0]['value'];
        }else{
            $orderinfo['orderstatus']='已下单';
        }
        result(1,'成功',$orderinfo);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_importorderlist(){
    return theme('warrior_import_orderlist');
}

function warrior_importorderexcel(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        die();
    } else {
        if (file_exists("sites/all/modules/warrior/uploadexcel/" . $_FILES["file"]["name"])) {
            echo '<script>alert("上传文件已存在！");location.href="?q=admin/importorderlist";</script>';
            die();
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "sites/all/modules/warrior/uploadexcel/" . $_FILES["file"]["name"]);
        }
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel5.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel2007.php');
    // $data = new Spreadsheet_Excel_Reader();
    // $data->read("excel/" . $_FILES["file"]["name"]);
    $reader = PHPExcel_IOFactory::createReader('Excel2007'); //设置以Excel5格式(Excel97-2003工作簿)
    $PHPExcel = $reader->load("sites/all/modules/warrior/uploadexcel/" . $_FILES["file"]["name"]); // 载入excel文件
    $sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
    $highestRow = $sheet->getHighestRow(); // 取得总行数
    $highestColumm = $sheet->getHighestColumn(); // 取得总列数
    /** 循环读取每个单元格的数据 */
    $num=0;
    $wrongnum=0;
    for ($row = 2; $row <= $highestRow; $row++){//行数是以第1行开始
        if($sheet->getCell('A'.$row)->getValue()){
            if(!$sheet->getCell('K'.$row)->getValue()){
                $k=0;
            }else{
                $k=$sheet->getCell('K'.$row)->getValue();
            }
            if(is_numeric($sheet->getCell('C'.$row)->getValue())){
                $ledgerdate=date('Y-m-d',(intval($sheet->getCell('C'.$row)->getValue()) - 25569) * 24*60*60);
            }else{
                $ledgerdate=$sheet->getCell('C'.$row)->getValue();
            }
            db_query("INSERT INTO warrior_userorder_list VALUES (NULL,'".$sheet->getCell('A'.$row)->getValue()."','".$sheet->getCell('B'.$row)->getValue()."','".$ledgerdate."','".$sheet->getCell('D'.$row)->getValue()."','".$sheet->getCell('E'.$row)->getValue()."','".$sheet->getCell('F'.$row)->getValue()."','".$sheet->getCell('G'.$row)->getValue()."','".$sheet->getCell('H'.$row)->getValue()."','".$sheet->getCell('I'.$row)->getValue()."','".$sheet->getCell('J'.$row)->getValue()."','".$k."','".$sheet->getCell('L'.$row)->getValue()."','".$sheet->getCell('M'.$row)->getValue()."','".$sheet->getCell('N'.$row)->getValue()."''".time()."')");
        }
    }
    echo '<script>alert("导入成功");location.href="?q=admin/importorderlist"</script>';
    exit();
}

function warrior_importuser(){
    return theme('warrior_import_user');
}

function warrior_importuserexcel(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        die();
    } else {
        // if (file_exists("sites/all/modules/warrior/userexcel/" . $_FILES["file"]["name"])) {
        //     echo '<script>alert("上传文件已存在！");location.href="/admin/importuser";</script>';
        //     die();
        // } else {
        move_uploaded_file($_FILES["file"]["tmp_name"], "sites/all/modules/warrior/userexcel/" . $_FILES["file"]["name"]);
        // }
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel5.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel2007.php');
    // $data = new Spreadsheet_Excel_Reader();
    // $data->read("excel/" . $_FILES["file"]["name"]);
    $reader = PHPExcel_IOFactory::createReader('Excel2007'); //设置以Excel5格式(Excel97-2003工作簿)
    $PHPExcel = $reader->load("sites/all/modules/warrior/userexcel/" . $_FILES["file"]["name"]); // 载入excel文件
    $sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
    $highestRow = $sheet->getHighestRow(); // 取得总行数
    $highestColumm = $sheet->getHighestColumn(); // 取得总列数
    /** 循环读取每个单元格的数据 */
    $num=0;
    $wrongnum=0;
    for ($row = 2; $row <= $highestRow; $row++){//行数是以第1行开始
        $name=db_query("SELECT field_employeesinfo_id_value FROM field_data_field_employeesinfo_id WHERE field_employeesinfo_id_value='".$sheet->getCell('A'.$row)->getValue()."'")->fetchColumn();
        if(!$name && $sheet->getCell('A'.$row)->getValue() && $sheet->getCell('A'.$row)->getValue()!=''){
            $node = new stdClass;
            $node->type = "employeesinfo";
            $node->language = "und";
            if($sheet->getCell('G1')->getValue() && $sheet->getCell('G1')->getValue()!=''){
                $node->title = $sheet->getCell('B'.$row)->getValue();
                $node->field_employeesinfo_type['und'][0]['value'] = '零售商';
                $node->field_employeesinfo_id['und'][0]['value'] = $sheet->getCell('A'.$row)->getValue();
                $node->field_employeesinfo_wholesaler['und'][0]['value'] = $sheet->getCell('C'.$row)->getValue();
                $node->field_employeesinfo_wholesalerid['und'][0]['value'] = $sheet->getCell('D'.$row)->getValue();
                $node->field_employeesinfo_brand['und'][0]['value']=$sheet->getCell('E'.$row)->getValue();
                $node->field_employeesinfo_level['und'][0]['value']=$sheet->getCell('F'.$row)->getValue();
                $node->field_employeesinfo_mobile['und'][0]['value']=$sheet->getCell('G'.$row)->getValue();
                $node->field_employeesinfo_area['und'][0]['value']=$sheet->getCell('H'.$row)->getValue();
            }else{
                $node->title = $sheet->getCell('B'.$row)->getValue();
                $node->field_employeesinfo_type['und'][0]['value'] = '批发商';
                $node->field_employeesinfo_id['und'][0]['value'] = $sheet->getCell('A'.$row)->getValue();
                $node->field_employeesinfo_brand['und'][0]['value']=$sheet->getCell('C'.$row)->getValue();
                $node->field_employeesinfo_level['und'][0]['value']=$sheet->getCell('D'.$row)->getValue();
                $node->field_employeesinfo_mobile['und'][0]['value']=$sheet->getCell('E'.$row)->getValue();
                $node->field_employeesinfo_area['und'][0]['value']=$sheet->getCell('F'.$row)->getValue();
            }
            $node->field_employeesinfo_integral['und'][0]['value'] = 0;
            $node->field_employeesinfo_rebate['und'][0]['value'] = 0;
            node_save($node);
        }else{
            $nid=db_query("SELECT entity_id FROM field_data_field_employeesinfo_id WHERE field_employeesinfo_id_value='".$sheet->getCell('A'.$row)->getValue()."'")->fetchColumn();
            $userinfo=node_load($nid);
            if($sheet->getCell('G1')->getValue() && $sheet->getCell('G1')->getValue()!=''){
                $userinfo->title = $sheet->getCell('B'.$row)->getValue();
                $userinfo->field_employeesinfo_wholesaler['und'][0]['value'] = $sheet->getCell('C'.$row)->getValue();
                $userinfo->field_employeesinfo_wholesalerid['und'][0]['value'] = $sheet->getCell('D'.$row)->getValue();
                $userinfo->field_employeesinfo_brand['und'][0]['value']=$sheet->getCell('E'.$row)->getValue();
                $userinfo->field_employeesinfo_level['und'][0]['value']=$sheet->getCell('F'.$row)->getValue();
                $userinfo->field_employeesinfo_mobile['und'][0]['value']=$sheet->getCell('G'.$row)->getValue();
                $userinfo->field_employeesinfo_area['und'][0]['value']=$sheet->getCell('H'.$row)->getValue();
            }else{
                $userinfo->title = $sheet->getCell('B'.$row)->getValue();
                $userinfo->field_employeesinfo_brand['und'][0]['value']=$sheet->getCell('C'.$row)->getValue();
                $userinfo->field_employeesinfo_level['und'][0]['value']=$sheet->getCell('D'.$row)->getValue();
                $userinfo->field_employeesinfo_mobile['und'][0]['value']=$sheet->getCell('E'.$row)->getValue();
                $userinfo->field_employeesinfo_area['und'][0]['value']=$sheet->getCell('F'.$row)->getValue();
            }
            node_save($userinfo);
        }
    }
    echo '<script>alert("导入成功");location.href="?q=admin/importuser"</script>';
    exit();
}

function warrior_importuserintegral(){
    return theme('warrior_import_userintegral');
}

function warrior_importuserintegralexcel(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        die();
    } else {
        if (file_exists("sites/all/modules/warrior/integralexcel/" . $_FILES["file"]["name"])) {
            echo '<script>alert("上传文件已存在！");location.href="?q=admin/importuserintegral";</script>';
            die();
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "sites/all/modules/warrior/integralexcel/" . $_FILES["file"]["name"]);
        }
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel5.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel2007.php');
    // $data = new Spreadsheet_Excel_Reader();
    // $data->read("excel/" . $_FILES["file"]["name"]);
    $reader = PHPExcel_IOFactory::createReader('Excel2007'); //设置以Excel5格式(Excel97-2003工作簿)
    $PHPExcel = $reader->load("sites/all/modules/warrior/integralexcel/" . $_FILES["file"]["name"]); // 载入excel文件
    $sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
    $highestRow = $sheet->getHighestRow(); // 取得总行数
    $highestColumm = $sheet->getHighestColumn(); // 取得总列数
    /** 循环读取每个单元格的数据 */
    $num=0;
    $wrongnum=0;
    for ($row = 2; $row <= $highestRow; $row++){//行数是以第1行开始
        if($sheet->getCell('A'.$row)->getValue() && $sheet->getCell('B'.$row)->getValue() && $sheet->getCell('A'.$row)->getValue()!='' && $sheet->getCell('B'.$row)->getValue()!=''){

            $userid=db_query("SELECT entity_id FROM field_data_field_employeesinfo_id WHERE field_employeesinfo_id_value='".$sheet->getCell('A'.$row)->getValue()."'")->fetchColumn();
            if($userid){
                $node=node_load($userid);
                $node->field_employeesinfo_integral['und'][0]['value'] = $node->field_employeesinfo_integral['und'][0]['value']+$sheet->getCell('B'.$row)->getValue();
                node_save($node);
                db_query("INSERT INTO warrior_user_integral_list VALUES (NULL,'积分导入','".$sheet->getCell('A'.$row)->getValue()."','','".$sheet->getCell('B'.$row)->getValue()."','".time()."','".$node->field_employeesinfo_integral['und'][0]['value']."')");
            }
        }
    }
    echo '<script>alert("导入成功");location.href="?q=admin/importuserintegral"</script>';
    exit();
}

function warrior_importuserprice(){
    return theme('warrior_import_userprice');
}

function warrior_importuserpriceexcel(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        die();
    } else {
        if (file_exists("sites/all/modules/warrior/priceexcel/" . $_FILES["file"]["name"])) {
            echo '<script>alert("上传文件已存在！");location.href="?q=admin/importuserprice";</script>';
            die();
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "sites/all/modules/warrior/priceexcel/" . $_FILES["file"]["name"]);
        }
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel5.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel2007.php');
    // $data = new Spreadsheet_Excel_Reader();
    // $data->read("excel/" . $_FILES["file"]["name"]);
    $reader = PHPExcel_IOFactory::createReader('Excel2007'); //设置以Excel5格式(Excel97-2003工作簿)
    $PHPExcel = $reader->load("sites/all/modules/warrior/priceexcel/" . $_FILES["file"]["name"]); // 载入excel文件
    $sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
    $highestRow = $sheet->getHighestRow(); // 取得总行数
    $highestColumm = $sheet->getHighestColumn(); // 取得总列数
    /** 循环读取每个单元格的数据 */
    $num=0;
    $wrongnum=0;
    for ($row = 2; $row <= $highestRow; $row++){//行数是以第1行开始
        if($sheet->getCell('A'.$row)->getValue() && $sheet->getCell('B'.$row)->getValue() && $sheet->getCell('C'.$row)->getValue() && $sheet->getCell('A'.$row)->getValue()!='' && $sheet->getCell('B'.$row)->getValue()!='' && $sheet->getCell('C'.$row)->getValue()!=''){
            $userid=db_query("SELECT entity_id FROM field_data_field_employeesinfo_id WHERE field_employeesinfo_id_value='".$sheet->getCell('A'.$row)->getValue()."'")->fetchColumn();
            if($userid){
                $node=node_load($userid);
                $node->field_employeesinfo_rebate['und'][0]['value'] = $node->field_employeesinfo_rebate['und'][0]['value']+round($sheet->getCell('C'.$row)->getValue(),2);
                $res=node_save($node);
            }
        }
    }
    echo '<script>alert("导入成功");location.href="?q=admin/importuserprice"</script>';
    exit();
}

function warrior_getproductorderlist(){
    $page=$_REQUEST['page'];
    $openid=$_REQUEST['openid'];
    $year=$_REQUEST['year'];
    $month=$_REQUEST['month'];
    $tradingtype=$_REQUEST['tradingtype'];
    $counterpartyid=$_REQUEST['counterpartyid'];
    $counterpartyname=$_REQUEST['counterpartyname'];
    $inout=$_REQUEST['inout'];
    $monthling=str_pad($month,2,'0',STR_PAD_LEFT);
    $userid=$_REQUEST['userid'];//新增
    if($page){
        $page=($page-1)*20;
    }else{
        $page=0;
    }
    if($year && $month){
        $timesql=" AND (ledgerdate like '".$year."/".$month."%' OR ledgerdate like '".$year."/".$monthling."%' OR ledgerdate like '".$year."-".$month."%' OR ledgerdate like '".$year."-".$monthling."%') ";
    }else if($year){
        $timesql=" AND ledgerdate like '".$year."%'";
    }else if($month){
        $timesql=" AND (ledgerdate like '%/".$month."/%' OR ledgerdate like '%/".$monthling."/%' OR ledgerdate like '%-".$month."-%' OR ledgerdate like '%-".$monthling."-%')";
    }else{
        $timesql="";
    }
    if($tradingtype){
        $tradingtypesql=" AND tradingtype='".$tradingtype."'";
    }else{
        $tradingtypesql="";
    }
    if($counterpartyid){
        $counterpartyidsql=" AND wholesalerid='".$counterpartyid."'";
        $counterpartyidsqlpfs=" AND counterpartyid='".$counterpartyid."'";
    }else{
        $counterpartyidsql="";
    }
    if($counterpartyname){
        $counterpartynamesql=" AND wholesalername='".$counterpartyname."'";
        $counterpartynamesqlpfs=" AND counterpartyname='".$counterpartyname."'";
    }else{
        $counterpartynamesql="";
    }
    if($inout){
        $inoutsql=" AND `inout`='".$inout."'";
    }else{
        $inoutsql="";
    }
    $uid=db_query("SELECT a.nid FROM node a
                   INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                   WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
    $userinfo=node_load($uid);
    if($userinfo->field_employeesinfo_type['und'][0]['value']=='批发商'){
        $arrList=db_query("SELECT * FROM warrior_userorder_list WHERE wholesalerid='".$userinfo->field_employeesinfo_id['und'][0]['value']."'".$timesql.$tradingtypesql.$counterpartyidsqlpfs.$counterpartynamesqlpfs.$inoutsql." LIMIT $page,20")->fetchAll();
    }else{
        $arrList=db_query("SELECT * FROM warrior_userorder_list WHERE counterpartyid='".$userinfo->field_employeesinfo_id['und'][0]['value']."'".$timesql.$tradingtypesql.$counterpartyidsql.$counterpartynamesql.$inoutsql." LIMIT $page,20")->fetchAll();
    }
    result(1,'成功',$arrList);
}

function warrior_getproductcondition(){
    $arrlist['tradingtype'][0]='批发商移库';
    $arrlist['tradingtype'][1]='销售出库';
    $arrlist['tradingtype'][2]='购入收货';
    $arrlist['tradingtype'][3]='2017年底库存';
    result(1,'成功',$arrlist);
}

function warrior_getusergaininfo(){
    $openid=$_REQUEST['openid'];
    if($openid){
        $arrList=db_query("SELECT * FROM warrior_userinfo_list WHERE openid='".$openid."' ORDER BY status DESC")->fetchAll();
        result(1,'成功',$arrList);
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_addusergaininfo(){
    $openid=$_REQUEST['openid'];
    $name=$_REQUEST['name'];
    $mobile=$_REQUEST['mobile'];
    $province=$_REQUEST['province'];
    $city=$_REQUEST['city'];
    $area=$_REQUEST['area'];
    $address=$_REQUEST['address'];
    if($openid && $name && $mobile && $province && $city && $area && $address){
        db_query("INSERT INTO warrior_userinfo_list VALUES (NULL,'".$openid."','".$name."','".$mobile."','".$province."','".$city."','".$area."','".$address."','".time()."',0)");
        result(1,'成功');
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_editusergaininfo(){
    $id=$_REQUEST['id'];
    $openid=$_REQUEST['openid'];
    $name=$_REQUEST['name'];
    $mobile=$_REQUEST['mobile'];
    $province=$_REQUEST['province'];
    $city=$_REQUEST['city'];
    $area=$_REQUEST['area'];
    $address=$_REQUEST['address'];
    if($openid && $name && $mobile && $province && $city && $area && $address){
        db_query("UPDATE warrior_userinfo_list SET name='".$name."',mobile='".$mobile."',province='".$province."',city='".$city."',area='".$area."',address='".$address."' WHERE id='".$id."' AND openid='".$openid."'");
        result(1,'成功');
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_setdefaultaddress(){
    $openid=$_REQUEST['openid'];
    $id=$_REQUEST['id'];
    db_query("UPDATE warrior_userinfo_list SET status=0 WHERE openid='".$openid."'");
    db_query("UPDATE warrior_userinfo_list SET status=1 WHERE id='".$id."' AND openid='".$openid."'");
    result(1,'成功');
}

function warrior_deleteusergaininfo(){
    $id=$_REQUEST['id'];
    $openid=$_REQUEST['openid'];
    if($id && $openid){
        db_query("DELETE FROM warrior_userinfo_list WHERE id='".$id."' AND openid='".$openid."'");
        result(1,'成功');
    }else{
        result(40001,'缺少参数');
    }
}

function warrior_getuserintegrallist(){
    $openid=$_REQUEST['openid'];
    $year=$_REQUEST['year'];
    $month=$_REQUEST['month'];
    $change=$_REQUEST['change'];
    $action=$_REQUEST['action'];
    $userid=$_REQUEST['userid'];//新增
    if($change=='minus'){
        $changesql=" AND integralnum<0";
    }else if($change=='add'){
        $changesql=" AND integralnum>0";
    }
    if($action){
        $actionsql=" AND action='".$action."'";
    }
    $uid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$userid."'")->fetchColumn();
    $userinfo=node_load($uid);
    $arrList=db_query("SELECT * FROM warrior_user_integral_list WHERE counterpartyid='".$userinfo->field_employeesinfo_id['und'][0]['value']."'".$actionsql.$changesql."order by id ASC")->fetchAll();
    if($year && $month){
        foreach ($arrList as $key => $value) {
            $month=str_pad($month,2,"0",STR_PAD_LEFT);
            $ym=$year.'-'.$month;
            if($ym==date('Y-m',$value->creadted)){
                $arrList[$key]->created=date('Y-m-d H:i:s',$value->creadted);
            }else{
                unset($arrList[$key]);
            }
        }
    }else if($year){
        foreach ($arrList as $key => $value) {
            if($year==date('Y',$value->creadted)){
                $arrList[$key]->created=date('Y-m-d H:i:s',$value->creadted);
            }else{
                unset($arrList[$key]);
            }
        }
    }else{
        foreach ($arrList as $key => $value) {
            $arrList[$key]->created=date('Y-m-d H:i:s',$value->creadted);
        }
    }
    result(1,'成功',$arrList);
}

function warrior_importorderstatuslist(){
    return theme('warrior_import_orderstatuslist');
}

function warrior_importorderstatusexcel(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        die();
    } else {
        if (file_exists("sites/all/modules/warrior/orderstatusexcel/" . $_FILES["file"]["name"])) {
            echo '<script>alert("上传文件已存在！");location.href="?q=admin/importorderlist";</script>';
            die();
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "sites/all/modules/warrior/orderstatusexcel/" . $_FILES["file"]["name"]);
        }
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel5.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel2007.php');
    // $data = new Spreadsheet_Excel_Reader();
    // $data->read("excel/" . $_FILES["file"]["name"]);
    $reader = PHPExcel_IOFactory::createReader('Excel2007'); //设置以Excel5格式(Excel97-2003工作簿)
    $PHPExcel = $reader->load("sites/all/modules/warrior/orderstatusexcel/" . $_FILES["file"]["name"]); // 载入excel文件
    $sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
    $highestRow = $sheet->getHighestRow(); // 取得总行数
    $highestColumm = $sheet->getHighestColumn(); // 取得总列数
    /** 循环读取每个单元格的数据 */
    $num=0;
    $wrongnum=0;
    for ($row = 2; $row <= $highestRow; $row++){//行数是以第1行开始
        if($sheet->getCell('A'.$row)->getValue() && $sheet->getCell('B'.$row)->getValue()){
            $info=db_query("SELECT nid,type FROM node WHERE title='".$sheet->getCell('A'.$row)->getValue()."'")->fetchAll();
            if($info){
                $node=node_load($info[0]->nid);
                if($info[0]->type='exchangeorder'){
                    $node->field_exchangeorder_status['und'][0]['value']=$sheet->getCell('B'.$row)->getValue();
                    $node->field_exchangeorder_kdgs['und'][0]['value']=$sheet->getCell('C'.$row)->getValue();
                    $node->field_exchangeorder_kddh['und'][0]['value']=$sheet->getCell('D'.$row)->getValue();
                    node_save($node);
                }else if($info[0]->type='snapuporder'){
                    $node->field_snapuporder_status['und'][0]['value']=$sheet->getCell('B'.$row)->getValue();
                    $node->field_snapuporder_kdgs['und'][0]['value']=$sheet->getCell('C'.$row)->getValue();
                    $node->field_snapuporder_kddh['und'][0]['value']=$sheet->getCell('D'.$row)->getValue();
                    node_save($node);
                }
            }
        }
    }
    echo '<script>alert("导入成功");location.href="?q=admin/importorderstatuslist"</script>';
    exit();
}

function warrior_deletedata(){
    set_time_limit(0);
    $arrlist=db_query("SELECT * FROM node WHERE type='application'")->fetchAll();
    foreach ($arrlist as $key => $value) {
        node_delete($value->nid);
    }
    die('done');
}


function warrior_importusertotalintegral(){
    return theme('warrior_import_usertotalintegral');
}

function warrior_importusertotalintegralexcel(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    if ($_FILES["file"]["error"] > 0) {
        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        die();
    } else {
        if (file_exists("sites/all/modules/warrior/totalintegralexcel/" . $_FILES["file"]["name"])) {
            echo '<script>alert("上传文件已存在！");location.href="?q=admin/importusertotalintegral";</script>';
            die();
        } else {
            move_uploaded_file($_FILES["file"]["tmp_name"], "sites/all/modules/warrior/totalintegralexcel/" . $_FILES["file"]["name"]);
        }
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel5.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/Reader/Excel2007.php');
    // $data = new Spreadsheet_Excel_Reader();
    // $data->read("excel/" . $_FILES["file"]["name"]);
    $reader = PHPExcel_IOFactory::createReader('Excel2007'); //设置以Excel5格式(Excel97-2003工作簿)
    $PHPExcel = $reader->load("sites/all/modules/warrior/totalintegralexcel/" . $_FILES["file"]["name"]); // 载入excel文件
    $sheet = $PHPExcel->getSheet(0); // 读取第一個工作表
    $highestRow = $sheet->getHighestRow(); // 取得总行数
    $highestColumm = $sheet->getHighestColumn(); // 取得总列数
    /** 循环读取每个单元格的数据 */
    $num=0;
    $wrongnum=0;

    for ($row = 2; $row <= $highestRow; $row++){//行数是以第1行开始
        //&& $sheet->getCell('B'.$row)->getValue()
        if($sheet->getCell('A'.$row)->getValue() && $sheet->getCell('B'.$row)->getValue()){
            $userid=db_query("SELECT a.nid FROM node a
                           INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                           WHERE a.type='employeesinfo' AND a.title='".$sheet->getCell('B'.$row)->getValue()."' AND b.field_employeesinfo_id_value='".$sheet->getCell('A'.$row)->getValue()."'")->fetchColumn();

            if($userid){
                $node=node_load($userid);
                $node->field_employeesinfo_integral['und'][0]['value'] = $sheet->getCell('C'.$row)->getValue();
                node_save($node);
            }
        }
    }
    echo '<script>alert("导入成功");location.href="?q=admin/importusertotalintegral"</script>';
    exit();
}

function warrior_downloadllsjfscbbpage(){
    return theme('warrior_download_llsjfscbbpage');
}

function warrior_downloadllsjfscbb(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    $dateymdstart=$_GET['dateymdstart'];
    $dateymdend=$_GET['dateymdend'];
    $wheresql="";
    if($dateymdstart && $dateymdend){
        $timestampstart=strtotime($dateymdstart);
        $timestampend=strtotime($dateymdend)+86400;
        $wheresql=" WHERE creadted>$timestampstart AND creadted<$timestampend";
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/PHPExcel.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $arrlist = db_query("select * from warrior_user_integral_list".$wheresql." ORDER BY id ASC")->fetchAll();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '下单日期');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', '区域');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', '零售商id');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', '零售商名称');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', '上传积分数');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', '消耗积分数');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', '剩余积分数');
    $intI = 2;

    foreach ($arrlist as $key => $value) {
        $nid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$value->counterpartyid."'")->fetchColumn();
        $node=node_load($nid);
        if($node->field_employeesinfo_type['und'][0]['value']=='零售商'){
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $intI, date('Y-m-d H:i:s',$value->creadted));
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $intI, $node->field_employeesinfo_area['und'][0]['value']);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $intI, $value->counterpartyid);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $intI, $node->title);
            if($value->integralnum>0){
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $intI, $value->integralnum);
            }else{
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $intI, $value->integralnum);
            }
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $intI, $value->remainnum);
            $intI++;
        }
    }

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="零售商积分商城报表.xlsx"');  //日期为文件名后缀
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  //excel5为xls格式，excel2007为xlsx格式
    $objWriter->save('php://output');
    exit;
}

function warrior_downloadpffjfscbbpage(){
    return theme('warrior_download_pffjfscbbpage');
}

function warrior_downloadpffjfscbb(){
    header('Content-Type: text/html; charset=utf-8');
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    $dateymdstart=$_GET['dateymdstart'];
    $dateymdend=$_GET['dateymdend'];
    $wheresql="";
    if($dateymdstart && $dateymdend){
        $timestampstart=strtotime($dateymdstart);
        $timestampend=strtotime($dateymdend)+86400;
        $wheresql=" WHERE creadted>$timestampstart AND creadted<$timestampend";
    }
    include_once('sites/all/modules/warrior/Classes/PHPExcel/PHPExcel.php');
    include_once('sites/all/modules/warrior/Classes/PHPExcel/IOFactory.php');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $arrlist = db_query("select * from warrior_user_integral_list".$wheresql." ORDER BY id ASC")->fetchAll();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '下单日期');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', '区域');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', '批发商id');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', '批发商名称');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', '上传积分数');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', '消耗积分数');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', '剩余积分数');
    $intI = 2;

    foreach ($arrlist as $key => $value) {
        $nid=db_query("SELECT a.nid FROM node a
                       INNER JOIN field_data_field_employeesinfo_id b ON b.entity_id=a.nid
                       WHERE a.type='employeesinfo' AND b.field_employeesinfo_id_value='".$value->counterpartyid."'")->fetchColumn();
        $node=node_load($nid);
        if($node->field_employeesinfo_type['und'][0]['value']=='批发商'){
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $intI, date('Y-m-d H:i:s',$value->creadted));
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $intI, $node->field_employeesinfo_area['und'][0]['value']);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $intI, $value->counterpartyid);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $intI, $node->title);
            if($value->integralnum>0){
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $intI, $value->integralnum);
            }else{
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $intI, $value->integralnum);
            }
            $objPHPExcel->getActiveSheet()->setCellValue('G' . $intI, $value->remainnum);
            $intI++;
        }
    }

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="批发商积分商城报表.xlsx"');  //日期为文件名后缀
    header('Cache-Control: max-age=0');
    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  //excel5为xls格式，excel2007为xlsx格式
    $objWriter->save('php://output');
    exit;
}

function warrior_theme(){
    return array(
        'warrior_import_orderlist' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_import_orderlist',
        ),
        'warrior_import_user' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_import_user',
        ),
        'warrior_import_userintegral' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_import_userintegral',
        ),
        'warrior_import_userprice' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_import_userprice',
        ),
        'warrior_import_orderstatuslist' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_import_orderstatuslist',
        ),
        'warrior_import_usertotalintegral' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_import_usertotalintegral',
        ),
        'warrior_download_llsjfscbbpage' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_download_llsjfscbbpage',
        ),
        'warrior_download_pffjfscbbpage' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_download_pffjfscbbpage',
        ),
        'warrior_import_integralorderpage' => array(
            'arguments' => array('args' => NULL),
            'template' => 'template/warrior_import_integralorderpage',
        ),
    );
}

function result($success, $res, $array) {
    $result ['code'] = $success;
    $result ['msg'] = $res;
    $result ['data'] = $array;
    $result = json_encode ( $result );

    $callback = $_REQUEST["callback"];

    if ($callback == "")
        exit ( $result );
    else
        exit ( $callback . "({$result})" );
}

function base64pictopic($img){
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace('data:image/jpg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);

    $filename=uniqid() . '.png';
    $data = base64_decode($img);

    $filepic = 'sites/default/files/uploadpic/' . $filename;
    file_put_contents($filepic, $data);
    return $filename;
}

function httpGet($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_URL, $url);

    $res = curl_exec($curl);
    curl_close($curl);
    return $res;
}

function http_post($url,$param,$post_file=false){
    $oCurl = curl_init();
    if(stripos($url,"https://")!==FALSE){
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
    }
    if (is_string($param) || $post_file) {
        $strPOST = $param;
    } else {
        $aPOST = array();
        foreach($param as $key=>$val){
            $aPOST[] = $key."=".urlencode($val);
        }
        $strPOST =  join("&", $aPOST);
    }
    curl_setopt($oCurl, CURLOPT_URL, $url);
    curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($oCurl, CURLOPT_POST,true);
    curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST);
    $sContent = curl_exec($oCurl);
    $aStatus = curl_getinfo($oCurl);
    curl_close($oCurl);
    if(intval($aStatus["http_code"])==200){
        return $sContent;
    }else{
        return false;
    }
}
