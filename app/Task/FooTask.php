<?php
namespace App\Task;

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Crontab\Annotation\Crontab;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Utils\ApplicationContext;

class FooTask
{

    private $logger;

    public function execute()
    {
        $this->logger = ApplicationContext::getContainer(LoggerFactory::class)->get('log','default');
        $this->logger->info('定时任务记录的时间----------------'.date('Y-m-d H:i:s', time()));
    }
}
