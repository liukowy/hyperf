<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

return [
    //数据库支持多库配置，默认支持default
    'default' => [
        'driver' => env('DB_DRIVER', 'mysql'), //引擎
        'host' => env('DB_HOST', 'localhost'),
        'port' => env('DB_PORT', 3306),
        'database' => env('DB_DATABASE', 'hyperf'),
        'username' => env('DB_USERNAME', 'root'),
        'password' => env('DB_PASSWORD', ''),
        'charset' => env('DB_CHARSET', 'utf8'), //字符集
        'collation' => env('DB_COLLATION', 'utf8_unicode_ci'),
        'prefix' => env('DB_PREFIX', ''), //前缀;
        'pool' => [
            'min_connections' => 1, //连接池内最少连接数
            'max_connections' => 10,//连接池内最大连接数
            'connect_timeout' => 10.0,//连接等待超时时间
            'wait_timeout' => 3.0,//超时时间
            'heartbeat' => -1,//心跳
            'max_idle_time' => (float) env('DB_MAX_IDLE_TIME', 60),//最大闲置时间
        ],
        'cache' => [
            'handler' => Hyperf\ModelCache\Handler\RedisHandler::class, //只有redis才能缓存模型;
            'cache_key' => 'mc:%s:m:%s:%s:%s',//mc:缓存前缀:m:表名:主键 KEY:主键值
            'prefix' => 'default',//缓存前缀
            'ttl' => 3600 * 24,//超时时间
            'empty_model_ttl' => 600,//查询不到数据时的超时时间
            'load_script' => true,//Redis 引擎下 是否使用 evalSha 代替 eval
        ],
        'commands' => [
            'gen:model' => [
                'path' => 'app/Model',
                'force_casts' => true,
                'inheritance' => 'Model',
            ],
        ],
        //下面是PDO配置；自己照着文档写的；
        'options' => [
            // 框架默认配置
            PDO::ATTR_CASE => PDO::CASE_NATURAL,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
            PDO::ATTR_STRINGIFY_FETCHES => false,
            PDO::ATTR_EMULATE_PREPARES => false,
        ],
    ],
];
