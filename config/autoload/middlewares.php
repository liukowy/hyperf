<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

return [
    //对应autoload/server.php中的name;该配置仅应用在该 Server 中--
    'http' => [
        // 数组内配置您的全局中间件，顺序根据该数组的顺序
//        YourMiddleware::class
    ],
];
