<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Controller;

use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;
use Hyperf\HttpServer\Contract\RequestInterface;
use App\Server\UserService;
use App\Event\UserRegistered;
use Hyperf\Paginator\Paginator;
//使用控制
use Hyperf\Utils\ApplicationContext;
use App\JsonRpc\CalculatorServiceInterface;
use App\JsonRpc\MathValue;

class IndexController extends AbstractController
{
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @var \Hyperf\Contract\ConfigInterface
     */
    public function index(RequestInterface $request)
    {
        // 存在则返回，不存在则返回默认值 null
        $id = $request->route('id'); //获取路由参数；
        $ids = $request->input('ids');//获取正常的参数get,post之类的；
        return (string)$id;

        $result = new UserService();
        $this->eventDispatcher->dispatch(new UserRegistered($result));
        return '成功了';

        //获取cookie
        $request->getCookieParams('name','哈哈'); //name这个key是否存在，不存在就返回哈哈;
    }

    /**
     * 测试文件上传
     * function test
     * created on 2020/4/7 14:50
     * Editor  phpStorm
     */
    public function test(RequestInterface $request)
    {
        $file = $request->file('photo');

        if ($request->hasFile('phone')) {
            return '真的有照片';
        }

        //文件是否成功
        if ($request->file('photo')->isValid()) {
            // ...
        }

        //和laravel差不多；---测试分页
    }

    public function page(RequestInterface $request)
    {
        $currentPage = $request->input('page', 1);
        $perPage = $request->input('per_page', 2);
        $users = [
            ['id' => 1, 'name' => 'Tom'],
            ['id' => 2, 'name' => 'Sam'],
            ['id' => 3, 'name' => 'Tim'],
            ['id' => 4, 'name' => 'Joe'],
        ];
        //获取总数据；然后进行拆分;
        return new Paginator($users, (int) $perPage, (int) $currentPage);
    }

    //sesson使用redis做驱动
    public function saveSession(RequestInterface $request)
    {
        ini_set('session.save_handler','redis'); //原生设置驱动为redis
        ini_set('session.save_path','tcp://localhost:6379');//设置存储路径为6379；
        session_start();
        header("Content-type:text/html;charset=utf-8");
        if(isset($_SESSION['view'])){
            $_SESSION['view'] = $_SESSION['view'] + 1;
        }else{
            $_SESSION['view'] = 1;
        }
        echo "[view]{$_SESSION['view']}";
    }

    public function jsonRpc(RequestInterface $request)
    {
        $client = ApplicationContext::getContainer()->get(CalculatorServiceInterface::class);

        /** @var MathValue $result */
        $result = $client->sum(new MathValue(1), new MathValue(2));

        var_dump($result->value);
    }
}
