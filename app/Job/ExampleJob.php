<?php

declare(strict_types=1);

namespace App\Job;

use Hyperf\AsyncQueue\Job; //使用命令行生成
use Hyperf\Logger\Logger;
use Hyperf\Utils\ApplicationContext;

class ExampleJob extends Job
{
    public $log;

    public $params;

    /**
     * ExampleJob constructor.
     * @param $params
     * @param $del
     */
    public function __construct($params)
    {
        //这里最好时普通数据；不要使用携带IO对象，比如PDO对象；
        $this->params = $params;
    }

    public function handle()
    {
        $log = ApplicationContext::getContainer()->get(\Hyperf\Logger\LoggerFactory::class)->get('log','default');
        $log->info('这个是进入到了队列中来了');
    }
}
