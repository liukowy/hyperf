<?php
namespace App\Listener;

use App\Event\UserRegistered;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Logger\LoggerFactory;

class UserRegisteredListener implements ListenerInterface
{
    public function listen(): array
    {
        // 返回一个该监听器要监听的事件数组，可以同时监听多个事件
        return [
            UserRegistered::class,
        ];
    }

    /**
    * @var \Psr\Log\LoggerInterface
    */
    protected $logger;

    /**
     * 这里的ContainerInterface 是 Interface类；  interface ConfigInterface{}
     * UserRegisteredListener constructor.
     * @param ContainerInterface $a
     */
    public function __construct(LoggerFactory $loggerFactory)
    {
        $this->logger = $loggerFactory->get('log', 'default');
    }

    /**
     * @param UserRegistered $event
     * 这里的LoggerFactory是这样的
     *
     * public function __construct(ContainerInterface $container)
    {
    $this->container = $container;
    $this->config = $container->get(ConfigInterface::class);
    }
     *
     */
    public function process(object $event)
    {
        $this->logger->info("--------测试的事件监听------");
    }
}