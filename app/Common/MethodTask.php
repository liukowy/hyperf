<?php
namespace App\Common;

use Hyperf\Logger\LoggerFactory;
use Hyperf\Utils\Coroutine;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Task\TaskExecutor;
use Hyperf\Task\Task;

class MethodTask
{
    public function handle($cid)
    {
        sleep(10);
        $log = ApplicationContext::getContainer()->get(LoggerFactory::class)->get('log','default');
        $log->info('这里是定时任务的----------------'.$cid);

        return [
            'worker.cid' => $cid,
            // task_enable_coroutine 为 false 时返回 -1，反之 返回对应的协程 ID
            'task.cid' => Coroutine::id(),
        ];
    }
}