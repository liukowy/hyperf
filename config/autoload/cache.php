<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */
/**
 *
 */
return [
    'default' => [
        'driver' => Hyperf\Cache\Driver\RedisDriver::class,//缓存驱动，默认为 Redis
        'packer' => Hyperf\Utils\Packer\PhpSerializerPacker::class,//打包器
        'prefix' => 'c:',//缓存前缀
    ],
    //协程内存驱动;如果您需要将数据缓存到 Context 中，可以尝试此驱动。例如以下应用场景 Demo::get 会在多个地方调用多次，但是又不想每次都到 Redis 中进行查询。//剩下的继续使用;
    'co'=>[
        'driver' => Hyperf\Cache\Driver\CoroutineMemoryDriver::class,
        'packer' => Hyperf\Utils\Packer\PhpSerializerPacker::class,
    ],
];
