<?php

use Hyperf\Crontab\Crontab;

/**
 * name 定时任务的名称，可以为任意字符串，各个定时任务之间的名称要唯一。
 *rule定时任务的执行规则，在分钟级的定义时，与 Linux 的 crontab 命令的规则一致，在秒级的定义时，规则长度从 5 位变成 6 位，在规则的前面增加了对应秒级的节点，也就是 5 位时以分钟级规则执行，6 位时以秒级规则执行，如 则代表每 *5 秒执行一次。注意在注解定义时，规则存在 \ 符号时，需要进行转义处理，即填写 *\/5 * * * * *。
 *
 *callback定时任务的执行回调，即定时任务实际执行的代码，在通过配置文件定义时，这里需要传递一个 [$class, $method] 的数组，$class 为一个类的全称，$method 为 $class 内的一个 public 方法。当通过注解定义时，只需要提供一个当前类内的 public 方法的方法名即可，如果当前类只有一个 public 方法，您甚至可以不提供该属性。
 *
 *singleton 解决任务的并发执行问题，任务永远只会同时运行 1 个。但是这个没法保障任务在集群时重复执行的问题。
 *
 * onOneServer 多实例部署项目时，则只有一个实例会被触发。
 *mutexPool  互斥锁使用的 Redis 连接池。
 *
 */
return [
//    'enable'=>true,
//        (new Crontab())->setName('Foo')->setRule('*/5 * * * * *')->setCallback([App\Task\FooTask::class, 'execute'])->setMemo('这是一个示例的定时任务'),

//    ],
];