<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

return [
    'default' => [
        'driver' => Hyperf\AsyncQueue\Driver\RedisDriver::class,//暂时只支持redis驱动
        'channel' => 'queue',//队列前缀
        'timeout' => 2,//pop消息的超时时间
        'retry_seconds' => 5,//失败后重试间隔
        'handle_timeout' => 10,//消息处理超时时间
        'processes' => 1,//消费进程数
        'concurrent' => [
            'limit' => 5,//同时处理消息数;
        ],
    ],
];
